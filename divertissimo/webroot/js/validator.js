$(document).ready(function() {
    $('.input-group input[required], .input-group textarea[required], .input-group select[required]').on('keyup change', function() {
        validateElement(this);
    });

    $('.input-group input[required], .input-group textarea[required], .input-group select[required]').trigger('change');
});

function validateElement(element)
{
    var $form = $(element).closest('form'),
        $group = $(element).closest('.input-group'),
        $addon = $group.find('.input-group-addon'),
        $icon = $addon.find('span'),
        state = true;

    if (!$group.data('validate')) {
        state = $(element).val() ? true : false;
    }else if ($group.data('validate') == "email") {
        state = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test($(element).val())
    }else if($group.data('validate') == 'phone') {
        state = /^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/.test($(element).val())
    }else if ($group.data('validate') == "length") {
        state = $(element).val().length >= $group.data('length') ? true : false;
    }else if ($group.data('validate') == "number") {
        state = !isNaN(parseFloat($(element).val())) && isFinite($(element).val());
    }

    if ($group.data('same')) {
        if($(element).val().toLowerCase() != $("#"+$group.data('same')).val().toLowerCase()){
            state = false;
        }
    }

    if (state) {
        $addon.removeClass('danger');
        $addon.addClass('success');
        $icon.attr('class', 'glyphicon glyphicon-ok');
    }else{
        $addon.removeClass('success');
        $addon.addClass('danger');
        $icon.attr('class', 'glyphicon glyphicon-remove');
    }
    
    return state;
}

function contactFormIsValid()
{
    if ($("#contactForm").find('.input-group-addon.danger').length == 0) {
        return true;
    }else{
        return false;
    }
}