<?php
namespace App\Lib;

require_once(ROOT . DS . 'vendor' . DS  . 'tecnickcom' . DS . 'tcpdf' . DS . 'tcpdf.php');
use TCPDF;

class PDFGenerator
{
    private $output;
    private $name;
    
    function __construct($name = 'divertissimo.pdf', $output = 'I')
    {
        $this->output = $output;
        $this->name = $name;
    }
    
    public function confirmation($order, $customer)
    {
        $pdf = new DivertissimoPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->SetAuthor('Divertissimo');
        $pdf->SetTitle("Confirmation d'achat");

        $pdf->setPrintHeader(true);
        $pdf->setPrintFooter(false);

        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(30);

        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// ---------------------------------------------------------

        $pdf->SetFont('helvetica', '', 12);

        $pdf->AddPage();

        $pdf->generatePseudoHeader();
        $pdf->generateCustomerSection($customer);
        $pdf->generateInvoiceTable($order);

        if($order->shippingMethod == 'electronic'){
            $yPos = 15;
            foreach($order->items as $index => $ticket){
                for($i = 0; $i < (int)$ticket->quantity; $i++){
                    if($i % 3 == 0){
                        $pdf->AddPage();
                        $yPos = 15;
                    }

                    $pdf->generateTicket($ticket, $yPos, $i + 1);

                    $yPos += 90;
                }
            }
        }
        
        return $pdf->Output($this->name, $this->output);
    }
}

class DivertissimoPDF extends TCPDF {
    public function Header() {
        if ($this->page == 1) {
            $image_file = WWW_ROOT . 'img' . DS . 'logoPDF.png';
            $this->Image($image_file, 10, 10, 30, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        }
    }

    /*public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }*/

    public function generatePseudoHeader()
    {
        $this->SetFont('helvetica', '', 30);

        $this->Cell("180", 6, "Confirmation d'achat", '', 0, 'C', false);
        $this->Ln();
    }

    public function generateCustomerSection($contact)
    {
        $this->SetFont('helvetica', '', 12);

        $this->Cell("100", 6, $contact->customer_firstname . ' ' . $contact->customer_lastname, '', 0, 'L', false);
        $this->Ln();

        $this->Cell("100", 6, $contact->customer_address, '', 0, 'L', false);
        $this->Ln();

        $this->Cell("100", 6, $contact->customer_city .' , ' . $contact->customer_region, '', 0, 'L', false);
        $this->Ln();

        $this->Cell("100", 6, $contact->customer_postalcode, '', 0, 'L', false);
        $this->Ln();
        $this->Ln();

    }

    public function generateInvoiceTable($order)
    {
        $headers = array('Événement', 'Billet', 'Quantité', 'Prix');

        // Colors, line width and bold font
        $this->SetFillColor(255, 0, 0);
        $this->SetTextColor(255);
        $this->SetDrawColor(128, 0, 0);
        $this->SetLineWidth(0.3);
        $this->SetFont('', 'B');
        // Header
        $width = array(70, 70, 20, 20);
        $num_headers = count($headers);
        for($i = 0; $i < $num_headers; ++$i) {
            $this->Cell($width[$i], 7, $headers[$i], 1, 0, 'C', 1);
        }
        $this->Ln();
        // Color and font restoration
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetFont('');
        // Data
        $fill = 0;
        foreach($order->items as $ticket) {
            $this->Cell($width[0], 6, $ticket->getEventName(), 'LR', 0, 'L', $fill);
            $this->Cell($width[1], 6, $ticket->getShortReference(), 'LR', 0, 'L', $fill);
            $this->Cell($width[2], 6, $ticket->quantity, 'LR', 0, 'R', $fill);
            $this->Cell($width[3], 6, $ticket->unitPrice . ' $', 'LR', 0, 'R', $fill);
            $this->Ln();
            $fill=!$fill;
        }
        $this->Cell(array_sum($width), 0, '', 'T');

        $totalWidth = [120, 60];
        $this->Ln();

        $this->Cell($totalWidth[0], 6, 'Sous-total', 'TLR', 0, 'L', $fill);
        $this->Cell($totalWidth[1], 6, $order->subtotal . ' $', 'TLR', 0, 'L', $fill);
        $this->Ln();

        foreach($order->getPromoList() as $promo){
            $this->Cell($totalWidth[0], 6, 'Promotion - ' . $promo['Description'], 'LR', 0, 'L', $fill);
            $this->Cell($totalWidth[1], 6, '-' . $promo['Amount'] . ' $', 'LR', 0, 'L', $fill);

            $this->Ln();
        }

        $this->Cell($totalWidth[0], 6, 'Frais de billetterie', 'LR', 0, 'L', $fill);
        $this->Cell($totalWidth[1], 6, $order->getFormattedFees(), 'LR', 0, 'L', $fill);
        $this->Ln();

        $this->Cell($totalWidth[0], 6, 'Livraison - ' . $order->getShippingMethodName(), 'LR', 0, 'L', $fill);
        $this->Cell($totalWidth[1], 6, $order->shippingFees . ' $', 'LR', 0, 'L', $fill);
        $this->Ln();

        $this->Cell($totalWidth[0], 6, 'TPS', 'LR', 0, 'L', $fill);
        $this->Cell($totalWidth[1], 6, $order->tps . ' $', 'LR', 0, 'L', $fill);
        $this->Ln();

        $this->Cell($totalWidth[0], 6, 'TVQ', 'LR', 0, 'L', $fill);
        $this->Cell($totalWidth[1], 6, $order->tvq . ' $', 'LR', 0, 'L', $fill);
        $this->Ln();

        $this->Cell($totalWidth[0], 6, 'Total', 'LR', 0, 'L', $fill);
        $this->Cell($totalWidth[1], 6, $order->total . ' $', 'LR', 0, 'L', $fill);
        $this->Ln();

        $this->Cell($totalWidth[0] + $totalWidth[1], 6, 'Mode de paiement : ' . $order->getPaymentModeName(), 'TLR', 0, 'L', $fill);
        $this->Ln();
        $this->Cell($totalWidth[0] + $totalWidth[1], 6, 'Code de confirmation : ' . implode(" - ", $order->getConfirmationCodes()), 'TLR', 0, 'L', $fill);
        $this->Ln();

        $this->Cell(array_sum($width), 0, '', 'T');
    }

    public function generateTicket($ticket, $yPos = 15, $order = '')
    {
        $image_file = WWW_ROOT . 'img' . DS . 'events' . DS . $ticket->eventId . DS . 'ticket.jpg';
        $this->Image($image_file, 15, $yPos, 0, 0, 'JPG', '', '', false, null, '', false, false, 1, false, false, false);

        $this->writeDate($yPos, $ticket->getFullReference());
        $this->writeLocation($yPos, $ticket->eventLocation);
        $this->writeValue($yPos, $ticket->unitPrice);
        $this->writeCode($yPos, $ticket->transaction_code, $order);
    }

    private function writeDate($yPos, $date)
    {
        $this->SetFont('helvetica', '', 12);

        $this->SetXY(18, $yPos + 12, true);

        $this->Write(10, $date);
    }

    private function writeLocation($yPos, $location)
    {
        $this->SetFont('helvetica', '', 12);

        $this->SetXY(18, $yPos + 18, true);

        $this->Write(10, $location);
    }

    private function writeValue($yPos, $value)
    {
        $this->SetFont('helvetica', '', 26);

        $this->SetXY(164, $yPos + 48, true);

        $this->Write(10, $value . ' $');
    }

    private function writeCode($yPos, $code, $order)
    {
        $this->SetFont('helvetica', '', 10);

        $this->SetXY(170, $yPos + 58, true);

        $this->Write(10, $code . '-' . $order);
    }
}