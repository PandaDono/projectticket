<?php

namespace App\Lib;

class OrderItem
{
    public $quantity;
    public $ticketId;
    public $ticketGradeName;
    public $ticketGradeId;
    public $ticketName;
    public $unitPrice;
    public $taxlessPrice;
    public $unitPromo = 0;
    public $promo;
    public $dateDisplay;
    public $subtotal;
    public $eventId;
    public $eventName;
    public $transaction_code;

    function  __construct($ticket, $quantity, $ticketGrade = null)
    {
        $this->quantity = $quantity;
        if(!$ticketGrade){
            $ticketGrade = $ticket->getGrade();
        }
        $this->feedPublicInfo($ticket, $ticketGrade, $ticketGrade->getDate());
    }

    public function getFullReference()
    {
        return $this->dateDisplay . " - " . $this->ticketGradeName . " - " . $this->ticketName;
    }

    public function getShortReference()
    {
        return substr($this->dateDisplay . " - " . $this->ticketName, 0 , 36);
    }
        
    public function getEventName()
    {
        return $this->eventName;
    }
    
    public function getPaidPrice()
    {
        return $this->unitPrice - $this->unitPromo;
    }
    
    public function getTotalPromo()
    {
        return $this->unitPromo * $this->quantity;
    }
    
    private function feedPublicInfo($ticket, $ticketGrade, $date)
    {
        $this->ticketId = $ticket->ticket_id;
        $this->eventId = $ticket->event_id;

        $this->ticketGradeName = $ticketGrade->ticket_grade_name;
        $this->ticketGradeId = $ticketGrade->ticket_grade_id;
        $this->ticketName = $ticket->ticket_name;
        $this->unitPrice = $ticket->ticket_price;
        $this->taxlessPrice = $ticket->getBasePrice();
        $this->dateDisplay = $date->Display;
        $event = $date->getEvent();
        $this->eventName = $event->event_name;
        $this->eventLocation = $event->getLocation();

        $this->subtotal = $this->quantity * $this->unitPrice;
    }
}