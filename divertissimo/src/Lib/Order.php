<?php

namespace App\Lib;

use Cake\ORM\TableRegistry;

class Order
{
    public $items = [];
    public $shippingMethod = 'pickup';

    public $subtotal;
    public $fees;
    public $feesTaxes;
    public $shippingFees = 0;
    public $tps;
    public $tvq;
    public $taxes;
    public $total;
    public $paymentMode;
    
    function __construct()
    {
    }

    public function getEvent()
    {

        $eventRepository = TableRegistry::get('Events');
        $event = $eventRepository->get($this->getEventId());
        
        return $event;
    }
    
    public function getEventId()
    {
        $sampleItem = $this->items[0];

        return $sampleItem->eventId;
    }
    
    public function addItem($item)
    {
        $this->items[] = $item;
    }
    
    public function calculateTotals($ticketsData)
    {
        $this->resetTotals();
        $ticketRepository = TableRegistry::get('Tickets');
        
        foreach($ticketsData as $ticketData){
            if(!$ticketData['Id'] || $ticketData['Qty'] < 1){
                continue;
            }

            $ticket = $ticketRepository->get($ticketData['Id']);

            $ticketBasePrice = $ticket->getBasePrice();
            $ticketFees = round($ticket->ticket_price * 0.039, 2) + 1.3;
            $feesTPS = round(($ticketFees) * 0.05, 2);
            $feesTVQ = round(($ticketFees) * 0.09975, 2);

            $ticketTPS = round(($ticketBasePrice) * 0.05, 2) + $feesTPS;
            $ticketTVQ = round(($ticketBasePrice) * 0.09975, 2) + $feesTVQ;

            $this->subtotal += number_format($ticketBasePrice * $ticketData['Qty'], 2);
            $this->fees += $ticketFees * $ticketData['Qty'];
            $this->tps += $ticketTPS * $ticketData['Qty'];
            $this->tvq += $ticketTVQ * $ticketData['Qty'];
            $this->feesTaxes += $feesTPS + $feesTVQ;
        }

        $this->subtotal = number_format($this->subtotal , 2);
        $this->fees = number_format($this->fees, 2);
        $this->tps = number_format($this->tps, 2);
        $this->tvq = number_format($this->tvq, 2);
        $this->taxes = number_format($this->tps + $this->tvq, 2);
        $this->total = number_format($this->subtotal + $this->fees + $this->shippingFees + $this->taxes, 2);
    }

    public function calculateTotalsForCheckout()
    {
        $this->resetTotals();
        $tickets = $this->items;

        foreach($tickets as $ticket){
            $ticketBasePrice = round(($ticket->unitPrice - $ticket->unitPromo) / 1.14975, 2);

            if($ticket->promo && $ticket->promo->promocode_include_fees){
                $ticketFees = 0;
            }else{
                $ticketFees = round(($ticket->unitPrice - $ticket->unitPromo) * 0.039, 2) + 1.3;
            }
            
            $feesTPS = round(($ticketFees) * 0.05, 2);
            $feesTVQ = round(($ticketFees) * 0.09975, 2);

            $ticketTPS = round(($ticketBasePrice) * 0.05, 2) + $feesTPS;
            $ticketTVQ = round(($ticketBasePrice) * 0.09975, 2) + $feesTVQ;

            $this->subtotal += number_format($ticketBasePrice * $ticket->quantity, 2);
            $this->fees += $ticketFees * $ticket->quantity;
            $this->tps += $ticketTPS * $ticket->quantity;
            $this->tvq += $ticketTVQ * $ticket->quantity;
            $this->feesTaxes += $feesTPS + $feesTVQ;
        }

        $this->subtotal = number_format($this->subtotal , 2);
        $this->fees = number_format($this->fees, 2);
        $this->tps = number_format($this->tps, 2);
        $this->tvq = number_format($this->tvq, 2);
        $this->taxes = number_format($this->tps + $this->tvq, 2);
        $this->total = number_format($this->subtotal + $this->fees + $this->shippingFees + $this->taxes, 2);
    }

    public function adjustShipping($shippingMethod)
    {
        $this->shippingMethod = $shippingMethod;

        if($shippingMethod == 'electronic'){
            $this->shippingFees = 0;
        }
        elseif($shippingMethod == 'pickup'){
            $this->shippingFees = 0;
        }
        elseif($shippingMethod == 'mail'){
            $this->shippingFees = 5;
        }
        elseif($shippingMethod == 'expressMail')
        {
            $this->shippingFees = 15;
        }else{
            $this->shippingFees = 0;
        }

        $this->calculateTotalsForCheckout();
    }
    
    public function getShippingMethodName()
    {
        return __($this->shippingMethod);
    }

    public function getPaymentModeName()
    {
        return __($this->paymentMode);
    }

    public function adjustPromo($amount)
    {
        $this->promo = $amount;

        $this->calculateTotalsForCheckout();
    }

    public function resetTotals()
    {
        $this->subtotal = 0;
        $this->fees = 0;
        $this->tps = 0;
        $this->tvq = 0;
    }
    
    public function getPromoList()
    {
        $promos = [];
        foreach($this->items as $item){
            $promo = $item->promo;
            if($promo){
                $promoId = $promo['promocode_id'];
                if(isset($promos[$promoId])){
                    $promos[$promoId]['Amount'] += $item->getTotalPromo();
                }else{
                    $promos[$promoId]['Amount'] = $item->getTotalPromo();
                    $promos[$promoId]['Description'] = $promo['promocode_message'];
                }
            }
        }
        
        return $promos;
    }
    
    public function getConfirmationCodes()
    {
        $codes = [];

        foreach($this->items as $item){
            $codes[] = $item->transaction_code;
        }

        return $codes;
    }

    public function generateFromTransaction($transaction)
    {
        $this->shippingMethod = $transaction->transaction_shipping_method;
        $this->subtotal = $transaction->transaction_subtotal;
        $this->fees = $transaction->transaction_fees;
        $this->feesTaxes = $transaction->transaction_fees_taxes;
        $this->shippingFees = $transaction->transaction_shipping;

        $this->taxes = $transaction->transaction_taxes;
        $this->total = $transaction->transaction_total;

        $this->paymentMode = $transaction->transaction_payment_mode;

        $this->tps = $this->extractTPS();
        $this->tvq = $this->extractTVQ();

        foreach($transaction->transaction_tickets as $item)
        {
            $this->generateItemFromTransactionItem($item);
        }
    }

    public function getFormattedFees()
    {
        if($this->fees == 0)
        {
            return __("Inclus");
        }else{
            return $this->fees . " $";
        }
    }
    
    private function extractTPS()
    {
        return number_format($this->subtotal * 0.05, 2);
    }

    private function extractTVQ()
    {
        return number_format($this->subtotal * 0.09975, 2);
    }

    private function generateItemFromTransactionItem($transactionItems)
    {
        $ticket = $transactionItems->getTicket();

        $item = new OrderItem($ticket, $transactionItems->ticket_quantity);
        $item->transaction_code = $transactionItems->transaction_ticket_code;

        $this->items[] = $item;
    }
}