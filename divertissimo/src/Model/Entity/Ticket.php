<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class Ticket extends Entity
{
    public function getBasePrice()
    {
        $price = $this->ticket_price;

        if($this->ticket_tax_included){
            $price = $this->removeTaxes($price);
        }

        return round($price, 2);
    }

    public function getEvent()
    {
        $eventRepository = TableRegistry::get("Events");
        $event = $eventRepository->get($this->event_id);

        return $event;
    }

    public function getEventName()
    {
        $event = $this->getEvent();

        return $event->event_name;
    }

    public function getGrade()
    {
        $gradeRepository = TableRegistry::get("TicketGrades");
        $grade = $gradeRepository->get($this->ticket_grade_id);

        return $grade;
    }

    public function getGradeName()
    {
        $grade = $this->getGrade();
        $gradeName = $grade->ticket_grade_name;

        return $gradeName;
    }

    public function getDate()
    {
        $grade = $this->getGrade();
        $date = $grade->getDate();

        return $date;
    }

    public function getDescription()
    {
        $eventName = $this->getEventName();
        $gradeName = $this->getGradeName();
        $ticketName = $this->ticket_name;
        $date = $this->getDate();

        return $eventName . ' - ' . $date->Display . ' - ' .   $gradeName . ' - ' . $ticketName;
    }

    protected function removeTaxes($price)
    {
        return $price / 1.14975;
    }

}