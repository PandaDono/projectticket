<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class Transaction extends Entity
{
    public function _getVerboseEmailSent()
    {
        if($this->transaction_email_sent){
            return __("Oui");
        }else{
            return __("Non");
        }
    }

    public function _getVerboseShipping()
    {
        if($this->transaction_tickets_sent){
            return __("Oui");
        }elseif($this->transaction_shipping_method == "express"){
            return "<strong style='color:red'>" . __($this->transaction_shipping_method) . "</strong>";
        }elseif($this->transaction_shipping_method == "mail"){
            return "<strong>" . __($this->transaction_shipping_method) . "</strong>";
        }else{
            return __("N/A");
        }
    }

    public function _getDate()
    {
        return $this->transaction_date->format('Y-m-d');
    }
}