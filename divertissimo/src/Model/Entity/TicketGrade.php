<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class TicketGrade extends Entity
{
    public function getUnsoldTickets()
    {
        $setAsideRepo = TableRegistry::get('SetAsides');
        $setAsideQty = $setAsideRepo->find('CountForTicketGrade', ['ticketGradeId' => $this->ticket_grade_id]);
        $unsoldQuantity = $this->ticket_grade_unsold;

        return $unsoldQuantity - $setAsideQty;
    }

    public function checkTicketsAvailable($quantity)
    {
        if(!$this->ticket_grade_total){
            return true;
        }else{
            $unsoldTickets = $this->getUnsoldTickets();
            if($unsoldTickets >= $quantity){
                return true;
            }else{
                return false;
            }
        }
    }

    public function getDate()
    {
        $dateRepo = TableRegistry::get('Dates');
        return $dateRepo->get($this->date_id);
    }

}