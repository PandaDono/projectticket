<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class SpecialField extends Entity
{
    public function _getName()
    {
        return 'special_' . $this->special_field_id . '_' . $this->special_field_name;
    }
    
    public function getSelectOptions()
    {
        $choices = $this->special_field_choices;

        $options = [];

        foreach($choices as $index=>$choice){
            $options[$choice->special_field_choice_key] = $choice->special_field_choice_value;
        }
        
        return $options;
    }
}