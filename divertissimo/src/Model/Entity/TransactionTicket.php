<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class TransactionTicket extends Entity
{
    public function getTicket()
    {
        $ticketRepository = TableRegistry::get("Tickets");
        $ticket = $ticketRepository->get($this->ticket_id);
        
        return $ticket;
    }
    
    public function getTicketDescription()
    {
        $ticket = $this->getTicket();
        return $ticket->getDescription();
    }
}