<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class Date extends Entity
{
    public function getEvent()
    {
        $eventsRepo = TableRegistry::get('Events');
        return $eventsRepo->get($this->event_id);
    }

    public function _getDisplay()
    {
        if($this->date_display){
            return $this->date_display;
        }else{
            if($this->date_date){
                return $this->date_date->format('Y-m-d');
            }else{
                return "";
            }
        }
    }
}