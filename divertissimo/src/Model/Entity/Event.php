<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class Event extends Entity
{
    public function getPriceRange()
    {
        $ticketRepository = TableRegistry::get('Tickets');

        $tickets = $ticketRepository->find()
            ->select(['ticket_price'])
            ->where(['event_id' => $this->event_id, 'ticket_price >' => 0 ])
            ->orderAsc('ticket_price')
            ->ToArray();

        $low = $tickets[0];
        $high = end($tickets);
        
        return $low['ticket_price'] . '-' . $high['ticket_price'];
    }

    public function getLocation()
    {
        $placeRepository = TableRegistry::get('Places');
        $place = $placeRepository->get($this->place_id);

        return $place->place_name . ', ' . $place->place_city;
    }
}