<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;

class TicketsTable extends Table
{
	public function initialize(array $config)
	{
		$this->table('tickets');
		$this->primaryKey('ticket_id');

		$this->belongsTo('TicketGrade', ['className' => 'TicketGrades']);
	}
}