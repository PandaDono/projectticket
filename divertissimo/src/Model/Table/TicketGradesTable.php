<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;

class TicketGradesTable extends Table
{
	public function initialize(array $config)
	{
		$this->table('ticket_grades');
		$this->primaryKey('ticket_grade_id');

		$this->belongsTo('Date', ['className' => 'Dates']);
		$this->hasMany('Ticket', ['className' => 'Tickets']);
	}

	public function findAllForDate(Query $query, array $options){
		$query->where([
			'date_id' => $options['DateId'],
		]);
		;
		return $query->all();
	}
}