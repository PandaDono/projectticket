<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;

class PlacesTable extends Table
{
	public function initialize(array $config)
	{
		$this->table('places');
		$this->primaryKey('place_id');

		$this->hasMany('Event', ['className' => 'Events']);
	}

	public function findByName(Query $query, array $options)
	{
		$query->where([
			'Places.place_name' => $options['name']
		]);

		$result = $query->all();
		return $result;
	}
}