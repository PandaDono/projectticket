<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;

class PromocodesTable extends Table
{
	public function initialize(array $config)
	{
		$this->table('promocodes');
		$this->primaryKey('promocode_id');

		$this->hasMany('promoTickets', ['className' => 'PromoTickets']);
	}

	public function findByCodeAndTicket(Query $query, array $options)
	{
		$query->where([
			'Promocodes.promocode_code' => $options['code'],
			'Promocodes.ticket_id' => $options['ticketId']
		]);
		
		$result = $query->first();
		
		return $result;
	}
}