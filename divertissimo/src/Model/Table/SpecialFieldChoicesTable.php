<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;

class SpecialFieldChoicesTable extends Table
{
	public function initialize(array $config)
	{
		$this->table('special_field_choices');
		$this->primaryKey('special_field_choice_id');

		$this->belongsTo('SpecialField', ['className' => 'SpecialFields']);
	}
}