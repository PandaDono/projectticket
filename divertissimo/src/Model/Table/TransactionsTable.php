<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;

class TransactionsTable extends Table
{
	public function initialize(array $config)
	{
		$this->table('transactions');
		$this->primaryKey('transaction_id');

		$this->belongsTo('Customer', ['className' => 'Customers']);
		$this->hasMany('TransactionTickets', ['className' => 'TransactionTickets']);
	}

	public function findByEventForAdmin(Query $query, array $options)
	{
		$query->contain(['Customer']);

		if(!empty($options['searchString'])){

			$formattedSearch = "%". $options['searchString'] ."%";

			$query->where(['event_id' => $options['eventId'],
			'OR' =>[
				['transaction_date LIKE' => $formattedSearch],
				['Customer.customer_firstname LIKE' => $formattedSearch],
				['Customer.customer_lastname LIKE' => $formattedSearch]
			]
			]);
		}else{
			$query->where(['event_id' => $options['eventId']]);
		}

		return $query->all();
	}

	public function findCountByEventForAdmin(Query $query, array $options)
	{
		$query->contain(['Customer']);

		if(!empty($options['searchString'])){

			$formattedSearch = "%". $options['searchString'] ."%";

			$query->where(['event_id' => $options['eventId'],
				'OR' =>[
					['transaction_date LIKE' => $formattedSearch],
					['Customer.customer_firstname LIKE' => $formattedSearch],
					['Customer.customer_lastname LIKE' => $formattedSearch]
				]
			]);
		}else{
			$query->where(['event_id' => $options['eventId']]);
		}

		return $query->count();
	}

}