<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;

class DatesTable extends Table
{
	public function initialize(array $config)
	{
		$this->table('dates');
		$this->primaryKey('date_id');

		$this->belongsTo('Event', ['className' => 'Events']);
		$this->hasMany('TicketGrade', ['className' => 'TicketGrades']);
	}
}