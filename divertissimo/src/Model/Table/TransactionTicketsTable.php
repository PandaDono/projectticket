<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;

class TransactionTicketsTable extends Table
{
	public function initialize(array $config)
	{
		$this->table('transaction_tickets');
		$this->primaryKey('transaction_ticket_id');

		$this->belongsTo('Transaction', ['className' => 'Transactions']);
	}
}