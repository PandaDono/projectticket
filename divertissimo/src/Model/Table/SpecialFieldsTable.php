<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;

class SpecialFieldsTable extends Table
{
	public function initialize(array $config)
	{
		$this->table('special_fields');
		$this->primaryKey('special_field_id');

		$this->belongsTo('Event', ['className' => 'Events']);
		$this->hasMany('SpecialFieldChoices', ['className' => 'SpecialFieldChoices']);
	}
}