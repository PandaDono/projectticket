<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;

class PromotersTable extends Table
{
	public function initialize(array $config)
	{
		$this->table('promoters');
		$this->primaryKey('promoter_id');

		$this->hasMany('Event', ['className' => 'Events']);
	}

	public function findByName(Query $query, array $options)
	{
		$query->where([
			'Promoters.promoter_name' => $options['name']
		]);

		$result = $query->all();
		return $result;
	}
}