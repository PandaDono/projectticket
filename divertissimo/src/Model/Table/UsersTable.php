<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;

class UsersTable extends Table
{
	public function initialize(array $config)
	{
		$this->table('users');
		$this->primaryKey('id');
	}	
}