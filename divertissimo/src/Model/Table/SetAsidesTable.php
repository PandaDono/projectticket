<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;

class SetAsidesTable extends Table
{
	public function initialize(array $config)
	{
		$this->table('set_asides');
		$this->primaryKey('set_aside_id');
		$this->belongsTo('TicketGrade', ['className' => 'TicketGrades']);

		$this->addBehavior('Timestamp');
	}

	public function findCountForTicketGrade(Query $query, array $options){
		$now = time();
		$expirationDate = $now - (60 * 10);  //10 minutes

		$query->select(['qty' => $query->func()->sum('quantity')]);

		$query->where([
			'ticket_grade_id' => $options['ticketGradeId'],
			'created >' => $expirationDate
		]);

		$query->hydrate(false);
		$result = $query->toArray();
		return $result[0]['qty'];
	}
}