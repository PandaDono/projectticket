<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;

class EventsTable extends Table
{
	public function initialize(array $config)
	{
		$this->table('events');
		$this->primaryKey('event_id');

		$this->belongsTo('Category', ['className' => 'Categories']);
		$this->belongsTo('Promoter', ['className' => 'Promoters', 'joinType' => 'INNER']);
		$this->belongsTo('Place', ['className' => 'Places']);

		$this->hasMany('Date', ['className' => 'Dates']);
	}

	public function findByUrl(Query $query, array $options)
	{
		$query->contain(['Place', 'Promoter', 'Category', 'Date', 'Date.TicketGrade', 'Date.TicketGrade.Ticket']);

		$query->where([
			'Events.event_url' => $options['url'],
		]);

		$query->andWhere(['Events.event_isactive' => 1]);

		$result = $query->first();
		return $result;
	}

	public function findByName(Query $query, array $options)
	{
		$query->contain(['Place', 'Promoter', 'Category', 'Date']);

		$query->where([
			'Events.event_name' => $options['name'],
		]);

		$query->andWhere(['Events.event_isactive' => 1]);

		//$query = $this->addCategory($query, $options);

		$result = $query->all();
		return $result;
	}

	public function findByPlaceName(Query $query, array $options)
	{
		$query->contain(['Place', 'Promoter', 'Category', 'Date']);

		$query->where([
			'Place.place_name' => $options['name'],
		]);

		$query->andWhere(['Events.event_isactive' => 1]);

		//$query = $this->addCategory($query, $options);

		$result = $query->all();
		return $result;
	}

	public function findByPromoterName(Query $query, array $options)
	{
		$query->contain(['Place', 'Promoter', 'Category', 'Date']);

		$query->where([
			'Promoter.promoter_name' => $options['name'],
		]);

		$query->andWhere(['Events.event_isactive' => 1]);

		//$query = $this->addCategory($query, $options);

		$result = $query->all();
		return $result;
	}

	public function findByAnything(Query $query, array $options)
	{
		$query->contain(['Place', 'Promoter', 'Category', 'Date']);

		$query->where([
			'Promoter.promoter_name LIKE' => '%' . $options['name'] . '%',
		]);

		$query->orWhere([
			'Place.place_name LIKE' => '%' . $options['name'] . '%',
		]);

		$query->orWhere([
			'Events.event_name LIKE' => '%' . $options['name'] . '%',
		]);

		$query = $this->addCategory($query, $options);

		$query->andWhere(['Events.event_isactive' => 1]);

		$result = $query->all();
		return $result;
	}

	public function findAllActiveEvents(Query $query, array $options)
	{
		$query->contain(['Promoter', 'Place', 'Category']);

		$query->select(['event_name', 'Promoter.promoter_name', 'Place.place_name']);

		$query->where([
			'Events.event_isactive' => 1
		]);

		$query->hydrate(false);

		$result = $query->toArray();
		return $result;
	}

	public function findAllActiveCategories(Query $query, array $options)
	{
		$query->contain(['Category']);

		$query->select(['Category.category_id' ,'Category.category_name']);

		$query->where([
			'Events.event_isactive' => 1
		]);

		$query->hydrate(false);

		$result = $query->toArray();
		return $result;
	}

	private function addCategory($query, $options)
	{
		if($options['category']){
			$query->andWhere(['Category.category_id' => $options['category']]);
		}

		return $query;
	}
}