<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;

class CustomersTable extends Table
{
	public function initialize(array $config)
	{
		$this->table('customers');
		$this->primaryKey('customer_id');
	}	
}