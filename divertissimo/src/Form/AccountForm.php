<?php
namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class AccountForm extends Form
{
    protected function _buildSchema(Schema $schema)
    {
        return $schema
            ->addField('firstname', 'string')
            ->addField('lastname', ['type' => 'string'])
            ->addField('phone', ['type' => 'string'])
            ->addField('address', ['type' => 'string'])
            ->addField('city', ['type' => 'string'])
            ->addField('country', ['type' => 'string'])
            ->addField('region', ['type' => 'string'])
            ->addField('postalCode', ['type' => 'string'])
            ->addField('email', ['type' => 'email'])
            ->addField('emailConfirm', ['type' => 'email'])
        ;
    }

    protected function _buildValidator(Validator $validator)
    {
        return $validator
            ->add('firstname', 'length', [
            'rule' => ['minLength', 10],
            'message' => 'Un nom est requis'
            ])
            ->add('email', 'format', [
            'rule' => 'email',
            'message' => 'Une adresse email valide est requise',
            ]);
    }

    protected function _execute(array $data)
    {
        return true;
    }
}