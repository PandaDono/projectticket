<section class="standard">
    <h2>Annulation d'achat</h2>
    <p><?= __("Votre achat a été annulé pendant la procédure de paiement via Paypal.") ?></p>
    <p><?= __("Si vous changez d'idée, vous pouvez recommencer le processus d'achat.") ?></p>

    <p>Pour toute question ou commentaire en lien avec les services de Divertissimo.ca, nous vous invitons à communiquer avec nous au : info@divertissimo.ca</p>
</section>