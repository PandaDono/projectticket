<section class="standard">
    <h2>Annulation d'achat</h2>
    <p><?= __("Le code de transaction Paypal a déjà été utilisé.") ?></p>
    <p><?= __("Si le problème persiste, veillez contacter l'administration de la plateforme.") ?></p>
</section>