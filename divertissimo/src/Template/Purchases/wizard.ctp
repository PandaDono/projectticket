<?php $this->start('bottomScript') ?>
    <?= $this->Html->script(['jquery.bootstrap.wizard.min', 'validator']) ?>
    <?= $this->Html->css(['validator']) ?>

    <script type="application/javascript">
        $(document).ready(function() {
            $('#rootwizard').bootstrapWizard(
                {'tabClass': 'nav nav-pills', onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = ($current/$total) * 100;
                $('#rootwizard').find('.bar').css({width:$percent+'%'});

                // If it's the last tab then hide the last button and show the finish instead
                if($current >= $total) {
                    $('#rootwizard').find('.pager .next').hide();
                    $('#rootwizard').find('.pager .finish').show();
                    $('#rootwizard').find('.pager .finish').removeClass('disabled');
                } else {
                    $('#rootwizard').find('.pager .next').show();
                    $('#rootwizard').find('.pager .finish').hide();
                }

            }, onTabClick: function(){return false;},
                onNext: function(tab, navigation, index){
                    if(index==3) {
                        if(!contactFormIsValid()){
                            $("#contactFormAlert").fadeIn( "slow", function() {
                                $("#contactFormAlert").fadeOut(3000);
                            });

                            return false;
                        }
                    }
                }
            });
        });
    </script>
<?php $this->end(); ?>

<div class="container standard">

    <div id="rootwizard">
        <div style="text-align:center;">
            <ul style="display: inline-block;">
                <li><a href="#reviewTab" data-toggle="tab"><?= __("Révision") ?></a></li>
                <li><a href="#deliveryTab" data-toggle="tab"><?= __("Mode de livraison") ?></a></li>
                <li><a href="#addressTab" data-toggle="tab"><?= __("Informations de contact") ?></a></li>
                <li><a href="#paymentTab" data-toggle="tab"><?= __("Paiement") ?></a></li>
            </ul>
        </div>

        <div class="tab-content">
            <?= $this->element('Wizard/recap') ?>
            <?= $this->element('Wizard/delivery') ?>
            <?= $this->element('Wizard/Account/account') ?>
            <?= $this->element('Wizard/payment') ?>
            
            <ul class="pager wizard">
                <li class="previous"><a href="#"><?= __("Précédent")?></a></li>
                <li class="next"><a href="#"><?= __("Suivant")?></a></li>
                <li class="next finish" style="display:none;"><a href="javascript:;"><?= __("Acheter")?></a></li>
            </ul>
        </div>
    </div>
    <div class="ticketSummary">
        <div id="totalsRow" class="row">
            <div class="col-md-12">
                <div class="col-sm-2 col-sm-offset-1 col-xs-6">
                    <span>Sous-Total</span> <br/>
                    <span id="subTotal"><?= $order->subtotal ?></span>$
                </div>
                <div class="col-sm-2 col-xs-6">
                    <span>Frais de billetterie</span> <br/>
                    <span id="feeTotal"><?= $order->getFormattedFees() ?></span>
                </div>
                <div class="col-sm-2 col-xs-6">
                    <span>Frais de livraison</span> <br/>
                    <span id="deliveryfees">0.00</span>$
                </div>
                <div class="col-sm-2 col-xs-6">
                    <span>Taxes</span> <br/>
                    <span id="taxTotal"><?= $order->taxes ?></span>$
                </div>
                <div class="col-sm-2 col-xs-6">
                    <span>Grand total</span> <br/>
                    <span id="grandTotal"><?= $order->total ?></span>$
                </div>
            </div>
        </div>
        <div id="loadingRow" class="row" style="display:none;">
            <div class="col-md-12" style=text-align:center;>
                <p><?= __('Recalcul des totaux...') ?></p>
            </div>
        </div>
    </div>
</div>

<div id="transactionModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="color:black;"><?= __("Transaction en cours") ?></h4>
            </div>
            <div class="modal-body">
                <p id="modalMessage" style="color:black;"><?= __("Veuillez patienter pendant que le système procède à la transaction") ?></p>
            </div>
        </div>
    </div>
</div>