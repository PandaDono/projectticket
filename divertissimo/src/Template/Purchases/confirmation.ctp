<section class="standard">
    <h2>Confirmation d'achat</h2>
    <p><?= __("Merci beaucoup d’avoir utilisé Divertissimo.ca pour l’achat de vos billets. 
    Nous espérons que vous avez apprécié votre expérience d’achat 
    et nous vous souhaitons un plaisir fou lorsque vous participerez à votre événement.") ?></p>
    <p><?= __("Votre code de confirmation est") ?> &nbsp; <strong><?= $codes ?></strong></p>

    <?php if($error): ?>
        <p><strong><?= $error ?></p></strong>
    <?php else: ?>
        <p><strong><?= __("Vous recevrez une courriel de confirmation sous peu") ?></p></strong>
    <?php endif ?>
    
    <a target="_blank" href="<?= $this->Url->build(['controller' => 'Purchases', 'action' => 'confirmationpdf']) ?>"><button style="margin:20px;" class="btn btn-primary">Afficher la confirmation de transaction</button></a>

    <p>Pour toute question ou commentaire en lien avec les services de Divertissimo.ca, nous vous invitons à communiquer avec nous au : info@divertissimo.ca</p>
</section>