<?php foreach($events as $event): ?>
	<div class="eventBox col-md-12">
		<div class="row">
			<div class="col-md-3">
				<?= $this->Html->image('events/' . $event->event_id. '/small.jpg'); ?>

				<p style="background-color:#3276b1; width:180px; text-transform: uppercase; text-align:center;"><?= $event->getPriceRange() ?> $</p>
			</div>

			<div class="col-md-4">
				<p style="font-weight:bold;"><?= $event['event_name'] ?></p>
				<p style="text-align: justify;"><?= $event['event_short_description'] ?></p>
			</div>

			<div class="col-md-4 pull-right">
				<p><?= $event->place['place_name'] ?></p>
				<p><?= $event['event_date_range'] ?></p>

				<a style="margin-top:20px;" href="<?= $event->event_url?>" class="btn-lg btn-primary btn"><?= __("ACHETER DES BILLETS") ?></a>
			</div>
		</div>
	</div>
<?php endforeach ?>

<?php if(count($events) == 0): ?>
	<div style="width:80%; margin:20px; padding:20px;">
		<p>Désolé, aucune événement correspondant à vos critères de recherche n'a été trouvé.</p>
	</div>
<?php endif; ?>

