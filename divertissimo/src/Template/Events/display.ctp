	<?php $this->start('bottomScript') ?>
		<script type="application/javascript">
			var eventJSON = <?= $jsonEvent ?>;
			$(document).ready(function(){
				$(".ticketDate").change(function(){
					updateGradeBox(this);
				})

				$(".ticketGrade").change(function(){
					updateTypeBox(this);
				})

				$(".ticketType").change(function(){
					updatePrice(this);
				})

				$(".ticketQuantity").change(function(){
					updateTotal(this)
				})

				$("#ticketsForm").submit(function(e){
					e.preventDefault();

					if(hasTickets()){
						submitOrder();
					}else{
						alert("Veuillez sélectionner au moins un billet d'un valeur supérieure à zéro");
					}

				})

				displayNextTicketBox(0);
				updateTotals();
			})

			function updateGradeBox(dateBox)
			{
				var ticketBox = $(dateBox).closest('.ticketBox');
				var fieldToUpdate = $(ticketBox).find('.ticketGrade');
				clearSelect(fieldToUpdate);

				var index = $(dateBox).val();
				var date = eventJSON.date[index];
				var grades = date.ticket_grade;

				$.each(grades, function(index, grade){
					if(grade.ticket_grade_active){
						var option = "<option value='" + index + "'>" + grade.ticket_grade_name + "</option>";
						$(fieldToUpdate).append(option)
					}
				})

				updateTypeBox(fieldToUpdate);
			}

			function updateTypeBox(gradeBox)
			{
				var ticketBox = $(gradeBox).closest('.ticketBox');
				var fieldToUpdate = $(ticketBox).find('.ticketType');
				clearSelect(fieldToUpdate);

				var dateBox = $(ticketBox).find('.ticketDate');
				var dateIndex = dateBox.val();
				var gradeIndex = $(gradeBox).val();

				var date = eventJSON.date[dateIndex];
				var grade = date.ticket_grade[gradeIndex];
				var types = grade.ticket;

				$.each(types, function(index, type){
					var option = "<option value='" + index + "'>" + type.ticket_name + "</option>";
					$(fieldToUpdate).append(option)
				})

				updatePrice(fieldToUpdate);
			}

			function updatePrice(typeBox)
			{
				var ticketBox = $(typeBox).closest('.ticketBox');
				var priceField = $(ticketBox).find('.ticketPrice');
				$(priceField).text("0.00 $");

				var dateBox = $(ticketBox).find('.ticketDate');
				var dateIndex = dateBox.val();

				var gradeBox = $(ticketBox).find('.ticketGrade');
				var gradeIndex = $(gradeBox).val();
				var typeIndex = $(typeBox).val();

				var date = eventJSON.date[dateIndex];
				var grade = date.ticket_grade[gradeIndex];
				var type = grade.ticket[typeIndex];

				$(priceField).text(type.ticket_price + " $");

				var ticketIdentifier = $(ticketBox).find('.ticketIdentifier');
				$(ticketIdentifier).val(type.ticket_id);

				var ticketValue = $(ticketBox).find('.ticketValue');
				$(ticketValue).val(type.ticket_price);

				//var quantityBox = $(ticketBox).find('.ticketQuantity');
				//updateTotal(quantityBox);
			}

			function updateTotal(quantityBox)
			{
				var ticketBox = $(quantityBox).closest('.ticketBox');
				var fieldToUpdate = $(ticketBox).find('.ticketTotal');
				$(fieldToUpdate).text("0.00 $");

				var dateBox = $(ticketBox).find('.ticketDate');
				var dateIndex = dateBox.val();

				var gradeBox = $(ticketBox).find('.ticketGrade');
				var gradeIndex = $(gradeBox).val();

				var typeBox = $(ticketBox).find('.ticketType');
				var typeIndex = $(typeBox).val();

				var date = eventJSON.date[dateIndex];
				var grade = date.ticket_grade[gradeIndex];
				var type = grade.ticket[typeIndex];

				var unitPrice = type.ticket_price;
				var quantity = $(quantityBox).val();

				var totalPrice = parseFloat(Math.round(unitPrice * quantity * 100) / 100).toFixed(2);
				$(fieldToUpdate).text(totalPrice + " $");

				if(quantity > 0){
					var ticketBoxIndex = $(".ticketBox").index(ticketBox);
					displayNextTicketBox(ticketBoxIndex + 1);
				}

				var stockQuantity = $(ticketBox).find('.stockQuantity');
				$(stockQuantity).val(quantity);

				updateTotals();
			}

			function updateTotals()
			{
				var calculatorUrl = "<?= $this->Url->build(['controller' => 'purchases', 'action' => 'totals']) ?>";

				$('#totalsRow').hide();
				$('#loadingRow').show();

				var tickets = getTickets();
				
				$.ajax({
					url: calculatorUrl,
					method: 'GET',
					data: {tickets:tickets},
					dataType: "json",
				}).done(function(result) {
					$("#subTotal").text(result.subtotal);
					$("#feeTotal").text(result.fees);
					$("#taxTotal").text(result.taxes);
					$("#grandTotal").text(result.total);

					$('#totalsRow').show();
					$('#loadingRow').hide();
				});
			}

			function submitOrder()
			{
				var validateUrl = "<?= $this->Url->build(['controller' => 'purchases', 'action' => 'validate']) ?>";
				var wizardUrl = "<?= $this->Url->build(['controller' => 'purchases', 'action' => 'wizard']) ?>";

				$(".spinning").show();

				var tickets = getTickets();

				$.ajax({
					url: validateUrl,
					method: 'POST',
					data: {tickets:tickets},
					dataType: "json",
				}).done(function(result) {
					document.location = wizardUrl;
				}).fail(function(error){
					console.log(error);
				}).always(function(){
					$(".spinning").hide();
				});
			}

			function getTickets()
			{
				var tickets = [];

				$(".ticketBox").each(function(){
					var id = $(this).find('.ticketIdentifier').val();
					var qty = $(this).find('.stockQuantity').val();
					var value = $(this).find('.ticketValue').val();

					var ticket = {Id:id, Qty:qty, Value:value}
					tickets.push(ticket);
				})

				return tickets;
			}

			function displayNextTicketBox(index)
			{
				var nextBox = $(".ticketBox").eq(index);
				$(nextBox).show();

				var dateBox = $(nextBox).find('.ticketDate');

				updateGradeBox(dateBox);
			}

			function clearSelect(mySelect)
			{
				$(mySelect)
					.find('option')
					.remove()
					.end();
			}

			function hasTickets()
			{
				var hasTickets = false;
				var tickets = getTickets();
				$(tickets).each(function(index, ticket){
					if(ticket.Qty > 0 && ticket.Value > 0){
						hasTickets = true;
					}
				})

				return hasTickets;
			}

			function totalIsZero()
			{
				$("#grandTotal").text();
			}

		</script>
	<?php $this->end(); ?>

	<div class="container">
		<section class="standard">
			<div id="eventImage">
				<?= $this->Html->image('events/' . $event->event_id. '/main.jpg', ['class' => 'img-responsive displayImage']); ?>
				<div id="categoryTag"><?= $event->category['category_name'] ?></div>
			</div>

			<div class="eventContent" style="padding:30px;">
				<div class="row">
					<div class="col-md-8">
						<span style="font-size:2em;"><strong><?= $event['event_name'] ?></strong></span>
						<br />
						<span><?= $event->promoter['promoter_name'] ?></span>
						<p style="margin-top:20px; text-align:justify;"><?= $event['event_description'] ?></p>
					</div>
					<div class="col-md-4">
						<div style="float:right;">
							<span><strong><?= $event->place['place_name'] ?></strong></span>
							<br />
							<span><?= $event->place['place_address'] ?>, <?= $event->place['place_city'] ?></span>
							<br />
							<span><?= $event->place['place_postalcode'] ?></span>
							<br />
							<span><?= $event->place['place_phone'] ?></span>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<h3 style="text-transform: uppercase; font-size:2em;"><?= __("Acheter des billets") ?></h3>
					</div>
				</div>

				<form id="ticketsForm">
					<div id="ticketTypes" style="margin:2em 0em;">
						<?= $this->element('ticketBox'); ?>
						<?= $this->element('ticketBox'); ?>
						<?= $this->element('ticketBox'); ?>
						<?= $this->element('ticketBox'); ?>
						<?= $this->element('ticketBox'); ?>
					</div>

					<button id="buyTicketsButton" type="submit" class="btn-success has-spinner">
						<span style="font-size:1.4em;"><?= __("Acheter") ?></span>
						<span class="glyphicon glyphicon-refresh spinning"  style="display:none; margin-left:20px;"></span>
					</button>
					<div style="clear:both;"></div>
				</form>

				<div class="ticketSummary">
					<div id="totalsRow" class="row">
						<div class="col-md-12">
							<div class="col-sm-3 col-xs-6">
								<span>Sous-Total</span> <br/>
								<span id="subTotal">0.00</span>$
							</div>
							<div class="col-sm-3 col-xs-6">
								<span>Frais de billetterie</span> <br/>
								<span id="feeTotal">0.00</span>$
							</div>
							<div class="col-sm-3 col-xs-6">
								<span>Taxes</span> <br/>
								<span id="taxTotal">0.00</span>$
							</div>
							<div class="col-sm-3 col-xs-6">
								<span>Grand total</span> <br/>
								<span id="grandTotal">0.00</span>$
							</div>
						</div>
					</div>
					<div id="loadingRow" class="row" style="display:none;">
						<div class="col-md-12" style=text-align:center;>
							<p><?= __('Recalcul des totaux...') ?></p>
						</div>
					</div>
				</div>
			</div>

		</section>
	</div>
