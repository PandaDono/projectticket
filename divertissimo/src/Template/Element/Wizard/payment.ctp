<?php $this->append('bottomScript') ?>
<script type="application/javascript">
    $(document).ready(function() {
        $("#paymentTypeBox input").on('change', function(){
            switchPaymentMode();
        })

        $("#promocode").on('blur', function(){
            applyPromoCode();
        })
        
        $("#paypalButton").click(function(){
            applyPromoCode(startPaypalTransaction());
        })

        $('#rootwizard .finish').click(function() {
            $('#rootwizard .finish').prop('disabled', true);

            applyPromoCode(prepareStripePayment());
        });

        switchPaymentMode();
    });

    function switchPaymentMode()
    {
        var paymentMode = $("input[name=paymentMode]:checked").val();

        if(paymentMode == 'creditCard'){
            $("#creditCardForm").show();
            $("#paypalButton").hide();
            $(".finish").show();
        }else if(paymentMode == 'paypal'){
            $("#creditCardForm").hide();
            $("#paypalButton").show();
            $(".finish").hide();
        }
    }

    function startPaypalTransaction()
    {
        var transactionUrl = "<?= $this->Url->build(['controller' => 'Transactions', 'action' => 'paypal']); ?>";
        var accountForm = $("#contactForm").serializeArray();
        $("body").css('cursor', 'wait');
        $("#paypalButton").attr("disabled","disabled");

        $.ajax({
            url: transactionUrl,
            method: 'post',
            data: {contact:accountForm},
            dataType: "json"
        }).done(function(result) {
            if(result.result == 'success'){
                document.location = result.link;
            }else{
                $("#modalMessage").text("<?= __("La transaction a été rejetée par le serveur de paiement.  Veuillez vérifier vos informations de paiement.") ?>");
            }
        }).fail(function(error){
            console.log(error);
        }).always(function(){
            $("body").css('cursor', 'default');
            $("#paypalButton").attr("disabled",false);
        });
    }

    function applyPromoCode(callback)
    {
        var promoUrl = "<?= $this->Url->build(['controller' => 'Purchases', 'action' => 'promocode'])?>"
        var code = $("#promocode").val();

        if(code.length == 0){
            return false;
        }

        $.ajax({
            url: promoUrl,
            method: 'GET',
            data: {code:code},
            dataType: "json",
        }).done(function(result) {
            if(result.success == true){
                $("#promoResult").css('color', 'green');
                updateTotalsWithPromo();
            }else{
                $("#promoResult").css('color', 'orange');
            }
            $("#promoResult").text(result.message);
            $("#promoResult").show();
        }).fail(function(error){
            console.log(error);
        }).always(function(){
            $(".spinning").hide();
            if(callback){
                callback();
            }
        });
    }

    function prepareStripePayment()
    {
        Stripe.setPublishableKey('<?= $stripeKey ?>');
        Stripe.card.createToken($("#creditCardForm"), stripeResponseHandler);
    }

    function stripeResponseHandler(status, response) {
        var $form = $("#creditCardForm");

        if (response.error) {
            var error = "";
            if(response.error.type == 'card_error'){
                error = "Numéro de carte de crédit invalide.  Veuillez valider les informations entrées."
            }else{
                error = response.error.message;
            }
            $form.find('.payment-errors').text(error);
            $('#rootwizard .finish').prop('disabled', false);
        } else { // Token was created!
            var token = response.id;
            processPayment(token);
        }
    };

    function processPayment(stripeToken){
        var transactionUrl = "<?= $this->Url->build(['controller' => 'Transactions', 'action' => 'stripe']); ?>";

        var accountForm = $("#contactForm").serializeArray();

        $('#transactionModal').modal('show');
        $("#modalMessage").html("<?= __("Veuillez patienter tandis que nous complétons la transaction") ?>");

        $.ajax({
            url: transactionUrl,
            method: 'post',
            data: {contact:accountForm, token:stripeToken},
            dataType: "json"
        }).done(function(result) {
            if(result == 'success'){
                document.location = "<?= $this->Url->build(['controller' => 'Purchases', 'action' => 'confirmation'])?>"
            }else{
                $("#modalMessage").text("<?= __("La transaction a été rejetée par le serveur de paiement.  Veuillez vérifier vos informations de paiement.") ?>");
            }
        }).fail(function(error){
            console.log(error);
        }).always(function(){

        });
    }

    function updateTotalsWithPromo(){
        var calculatorUrl = "<?= $this->Url->build(['controller' => 'purchases', 'action' => 'promototal']) ?>";

        $('#totalsRow').hide();
        $('#loadingRow').show();

        $.ajax({
            url: calculatorUrl,
            method: 'post',
            dataType: "json",
        }).done(function(result) {
            $("#subTotal").text(result.subtotal);
            $("#feeTotal").text(result.fees);
            $("#deliveryfees").text(result.shippingFees);
            $("#taxTotal").text(result.taxes);
            $("#grandTotal").text(result.total);
        }).fail(function(error){
            console.log("Erreur Wiz577");
            console.log(error);
        }).always(function(){
            $('#totalsRow').show();
            $('#loadingRow').hide();
        });
    }
</script>
<?php $this->end(); ?>

<section class="tab-pane" id="paymentTab">
    <h2><?= __("Paiement") ?></h2>
    <div class="row" style="padding:30px;">
        <div id="paymentTypeBox" class="col-sm-4 col-sm-offset-2" style="padding-left:30px;">
            <div class="form-group">
                <div class="radio">
                    <label><input type="radio" name="paymentMode" id="creditCard" value="creditCard" checked><?= __("Carte de crédit") ?></label>
                </div>
                <div class="radio">
                    <label><input type="radio" name="paymentMode" id="expressMail" value="paypal"><?= __("Paypal / Carte de crédit") ?></label>
                </div>
            </div>

            <div class="form-group" style="margin-top:90px;">
                <label for="promocode"><?= __("Code promotionnel") ?></label>
                <input type="text" class="form-control" id="promocode">
                <p id="promoResult" style="display:none;"></p>
            </div>
        </div>

        <div class="col-sm-4 col-sm-offset-1">
            <button id="paypalButton" class="btn btn-default btn-lg">Payer avec Paypal <img style="margin-left:16px;" src="https://www.paypalobjects.com/webstatic/mktg/logo/pp_cc_mark_37x23.jpg" /></button>

            <form id="creditCardForm">
                <div class="form-group">
                    <label for="cardName"><?= __("Type de carte") ?></label>
                    <select class="form-control" id="cardName">
                        <option value="visa">Visa</option>
                        <option value="mastercard">Mastercard</option>
                    </select>
                </div>
                
                <div class="form-group">
                    <label for="ownerFirstname"><?= __("Prénom du détenteur") ?></label>
                    <input type="text" class="form-control" id="ownerFirstname" value="">
                </div>

                <div class="form-group">
                    <label for="ownerLastname"><?= __("Nom du détenteur") ?></label>
                    <input type="text" class="form-control" id="ownerLastname" value="">
                </div>

                <div class="form-group">
                    <label for="creditCardNumber"><?= __("Numéro de carte") ?></label>
                    <input type="text" data-stripe="number" class="form-control" id="creditCardNumber" value="">
                </div>

                <div class="form-group">
                    <label for="creditCardSecurity"><?= __("Code de sécurité") ?></label>
                    <input type="text" class="form-control" id="creditCardSecurity" data-stripe="cvc" value="">
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <span style="font-weight:bold;">Expiration</span>
                    </div>
                    <div class="col-xs-4">
                        <select class="form-control" id="expirationMonth" data-stripe="exp_month">
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
                            <option value="08">08</option>
                            <option value="09">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select>
                    </div>
                    <div class="col-xs-4">
                        <select class="form-control" id="expirationYear" data-stripe="exp_year">
                            <option value="2016">2016</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <p class="payment-errors" style="color:red; padding-left:14px;"></p>
                </div>
            </form>
        </div>
    </div>
</section>