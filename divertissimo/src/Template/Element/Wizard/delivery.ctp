<?php $this->append('bottomScript') ?>
    <script type="application/javascript">
        $(document).ready(function() {
            $("#deliveryTab input:radio").on('change', function(){
                var method = $(this).val();
                updateTotalsWithShipping(method);
            });
        });
    
        function updateTotalsWithShipping(method){
            var calculatorUrl = "<?= $this->Url->build(['controller' => 'purchases', 'action' => 'shipping']) ?>";
    
            $('#totalsRow').hide();
            $('#loadingRow').show();
    
            $.ajax({
                url: calculatorUrl,
                method: 'post',
                data: {shippingMethod:method},
                dataType: "json",
            }).done(function(result) {
                $("#subTotal").text(result.subtotal);
                $("#feeTotal").text(result.fees);
                $("#deliveryfees").text(result.shippingFees);
                $("#taxTotal").text(result.taxes);
                $("#grandTotal").text(result.total);
            }).fail(function(error){
                console.log("Erreur Wiz2268");
                console.log(error);
            }).always(function(){
                $('#totalsRow').show();
                $('#loadingRow').hide();
            });
        }
    </script>
<?php $this->end(); ?>


<section class="tab-pane" id="deliveryTab">
    <h2><?= __("Mode de livraison") ?></h2>
    <div class="form-group">
        <div class="col-xs-12" style="margin-bottom:50px; margin-left:38%;">
            <?php if($event->event_paper_tickets): ?>
                <div class="radio">
                    <label> <input type="radio" name="delivery" id="electronic" value="electronic" checked><?= __("Billet électronique") ?> - <?= __("gratuit") ?></label>
                </div>
                <div class="radio">
                    <label> <input type="radio" name="delivery" id="pickup" value="pickup"><?= __("Ramassage au gichet") ?> - <?= __("gratuit") ?></label>
                </div>
                <div class="radio">
                    <label> <input type="radio" name="delivery" id="mail" value="mail"><?= __("Livraison par courrier") ?> - 5.00$</label>
                </div>
                <div class="radio">
                    <label> <input type="radio" name="delivery" id="expressMail" value="expressMail"><?= __("Livraison par courrier express") ?> - 15.00$</label>
                </div>
            <?php else: ?>
                <div class="radio">
                    <label> <input type="radio" name="delivery" id="pickup" value="pickup" checked><?= __("Ramassage au gichet") ?> - <?= __("gratuit") ?></label>
                </div>
            <?php endif ?>
        </div>
    </div>
</section>
