<section class="tab-pane" id="reviewTab">
    <h2><?= __("Votre achat")?></h2>
    <div id="recapTickets" class="row">

        <div class="col-md-9">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3">
                        <?= $this->Html->image('events/' . $event->event_id . '/small.jpg'); ?>
                    </div>

                    <div class="col-md-9">
                        <h3><?= $event->event_name ?></h3>
                        <p style="text-align: justify;"><?= $event->event_description ?></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <?php foreach($order->items as $item): ?>
                <div class="ticketSummary" style="margin-top:0px; padding:10px 20px;">
                    <p><?= $item->dateDisplay ?></p>
                    <p><?= $item->ticketGradeName ?> - <?= $item->ticketName ?></p>
                    <p><?= $item->quantity ?>  x <?= number_format($item->unitPrice, 2) ?> = <?= $item->subtotal ?>&nbsp;$</p>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</section>