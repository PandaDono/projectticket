<h2><?= __("Informations complémentaires") ?></h2>
    <form id="specialFieldsForm">
        <div class="row">
            <div class="col-sm-offset-4 col-sm-4">
                <?php foreach($specialFields as $specialField): ?>
                    <?php if($specialField->special_field_type == 'text'): ?>
                        <?= $this->element('Wizard/Account/text_field', ['field' => $specialField]) ?>
                    <?php endif ?>
    
                    <?php if($specialField->special_field_type == 'select'): ?>
                        <?= $this->element('Wizard/Account/select_field', ['field' => $specialField]) ?>
                    <?php endif ?>
                <?php endforeach; ?>
            </div>
        </div>
    </form>
