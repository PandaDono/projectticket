<div class="form-group">
    <label for="<?= $field->Name ?>" class="control-label"><?= __($field->special_field_label) ?></label>
    <div class="input-group">
        <?= $this->Form->select($field->Name, $field->getSelectOptions(),
            ['label' => false, 'class' => 'form-control']);
        ?>
        <span class="input-group-addon success"><span class="glyphicon glyphicon-ok"></span></span>
    </div>
</div>
