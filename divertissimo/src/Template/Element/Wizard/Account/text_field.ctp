<div class="form-group">
    <label for="<?= $field->Name ?>" class="control-label"><?= __($field->special_field_label) ?></label>
    <div class="input-group">
        <?= $this->Form->text($field->Name, ['label' => false, 'class' => 'form-control', 'required' => $field->special_field_required]); ?>
        <?php if($field->special_field_required): ?>
            <span class="input-group-addon danger"><span class="glyphicon glyphicon-remove"></span></span>
        <?php else: ?>
            <span class="input-group-addon success"><span class="glyphicon glyphicon-ok"></span></span>
        <?php endif ?>
    </div>
</div>