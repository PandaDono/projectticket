<?php $this->append('bottomScript') ?>
<script type="application/javascript">
    $(document).ready(function() {
        $("#showLoginButton").click(function(){
            $("#loginForm").show();
            $("#contactForm").hide();
        });

        $("#showSubscriptionFormButton").click(function(){
            $("#contactForm").show();
            $("#loginForm").hide();
        });

        $("#showSubscriptionFormButton").click(function(){
            $("#contactForm").show();
            $("#loginForm").hide();
        });

        $("#country").change(function(){
            $(".region").hide();
            toggleOtherRegion(false);

            var country = $(this).val();

            if(country == 'US'){
                $("#USRegions").show();
            }else if(country == "Canada"){
                $("#canadaRegions").show();
            }else{
                $("#otherRegions").show();
                toggleOtherRegion(true);
            }
        })

        toggleOtherRegion(false);
    });

    function toggleOtherRegion(required)
    {
        var group = $("#otherInfo").closest('.input-group');
        var addon = $(group).find('.input-group-addon');
        var icon = $(addon).find('span');

        $("#otherInfo").prop('required', required);
        if(required && $("#otherInfo").length == 0){
            $(addon).removeClass('success');
            $(addon).addClass('danger');
            $(icon).attr('class', 'glyphicon glyphicon-remove');
        }else{
            $(addon).removeClass('danger');
            $(addon).addClass('success');
            $(icon).attr('class', 'glyphicon glyphicon-ok');
        }
    }

</script>
<?php $this->end(); ?>

<section class="tab-pane" id="addressTab">
    <h2><?= __("Informations de contact") ?></h2>

    <?= $this->Form->create($accountForm, ['Id' => 'contactForm']); ?>
        <div class="row">
            <div class="col-sm-offset-4 col-sm-4">
                <div class="form-group">
                    <label for="firstname" class="control-label"><?= __("Prénom") ?></label>
                    <div class="input-group">
                        <?= $this->Form->input('firstname', ['label' => false, 'class' => 'form-control', 'placeholder' => __('Arthur'), 'required' => true]); ?>
                        <span class="input-group-addon danger"><span class="glyphicon glyphicon-remove"></span></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="lastname" class="control-label"><?= __("Nom") ?></label>
                    <div class="input-group">
                        <?= $this->Form->input('lastname', ['label' => false, 'class' => 'form-control', 'placeholder' => __('Laroche'), 'required' => true]); ?>
                        <span class="input-group-addon danger"><span class="glyphicon glyphicon-remove"></span></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="phone" class="control-label"><?= __("Téléphone") ?></label>
                    <div class="input-group">
                        <?= $this->Form->input('phone', ['label' => false, 'class' => 'form-control', 'placeholder' => __('555-555-5555'), 'required' => true]); ?>
                        <span class="input-group-addon danger"><span class="glyphicon glyphicon-remove"></span></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="address" class="control-label"><?= __("Adresse") ?></label>
                    <div class="input-group">
                        <?= $this->Form->input('address', ['label' => false, 'class' => 'form-control', 'placeholder' => __('10 rue du Granit'), 'required' => true]); ?>
                        <span class="input-group-addon danger"><span class="glyphicon glyphicon-remove"></span></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="city" class="control-label"><?= __("Ville") ?></label>
                    <div class="input-group">
                        <?= $this->Form->input('city', ['label' => false, 'class' => 'form-control', 'placeholder' => __('Ste-Granit'), 'required' => true]); ?>
                        <span class="input-group-addon danger"><span class="glyphicon glyphicon-remove"></span></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="country" class="control-label"><?= __("Pays") ?><label>
                    <div class="input-group">
                        <?= $this->Form->select('country',
                            [
                                'Canada' => __("Canada"),
                                'US' => __("États-Unis"),
                                'Other' => __("Autre")
                            ],
                            ['label' => false, 'class' => 'form-control', 'id' => 'country']);
                        ?>
                        <span class="input-group-addon success"><span class="glyphicon glyphicon-ok"></span></span>
                    </div>
                </div>

                <div class="form-group region" id="canadaRegions">
                    <label for="province" class="control-label"><?= __("Province") ?><label>
                    <div class="input-group">
                        <?= $this->Form->select('CanadaRegion',
                            [
                                'AB' =>  __("Alberta"),
                                'BC' => __("British Columbia"),
                                'MB' => __("Manitoba"),
                                'NB' => __("New Brunswick"),
                                'NL' => __("Newfoundland and Labrador"),
                                'NS' => __("Nova Scotia"),
                                'ON' => __("Ontario"),
                                'PE' => __("Prince Edward Island"),
                                'QC' => __("Quebec"),
                                'SK' => __("Saskatchewan"),
                                'NT' => __("Northwest Territories"),
                                'NU' => __("Nunavut"),
                                'YT' => __("Yukon")
                            ],
                            ['label' => false, 'class' => 'form-control', 'default' => 'QC']);
                        ?>
                        <span class="input-group-addon success"><span class="glyphicon glyphicon-ok"></span></span>
                    </div>
                </div>

                <div class="form-group region" id="USRegions" style="display:none;">
                    <label for="state" class="control-label"><?= __("État") ?><label>
                    <div class="input-group">
                        <?= $this->Form->select('USRegion',
                            [
                                'AL' =>  __("Alabama"),
                                'AK' => __("Alaska"),
                                'AZ' => __("Arizona"),
                                'AR' => __("Arkansas"),
                                'CA' => __("California"),
                                'CO' => __("Colorado"),
                                'CT' => __("Connecticut"),
                                'DE' => __("Delaware"),
                                'DC' => __("District Of Columbi"),
                                'FL' => __("Florida"),
                                'GA' => __("Georgia"),
                                'HI' => __("Hawaii"),
                                'ID' => __("Idaho"),
                                'IL' =>  __("Illinois"),
                                'IN' => __("Indiana"),
                                'IA' => __("Iowa"),
                                'KS' => __("Kansas"),
                                'KY' => __("Kentucky"),
                                'LA' => __("Louisiana"),
                                'ME' => __("Maine"),
                                'MD' => __("Maryland"),
                                'MA' => __("Massachusetts"),
                                'MI' => __("Michigan"),
                                'MN' => __("Minnesota"),
                                'MS' => __("Mississippi"),
                                'MO' => __("Missouri"),
                                'MT' =>  __("Montana"),
                                'NE' => __("Nebraska"),
                                'NV' => __("Nevada"),
                                'NH' => __("New Hampshire"),
                                'NJ' => __("New Jersey"),
                                'NM' => __("New Mexico"),
                                'NY' => __("New York"),
                                'NC' => __("North Carolina"),
                                'ND' => __("North Dakota"),
                                'OH' => __("Ohio"),
                                'OK' => __("Oklahoma"),
                                'OR' => __("Oregon"),
                                'PA' => __("Pennsylvania"),
                                'RI' =>  __("Rhode Island"),
                                'SC' => __("South Carolina"),
                                'SD' => __("South Dakot"),
                                'TN' => __("Tennessee"),
                                'TX' => __("Texas"),
                                'UT' => __("Utah"),
                                'VT' => __("Vermont"),
                                'VA' => __("Virginia"),
                                'WA' => __("Washington"),
                                'WV' => __("West Virginia"),
                                'WI' => __("Wisconsin"),
                                'WY' => __("Wyoming")
                            ],
                            ['label' => false, 'class' => 'form-control', 'default' => 'QC']);
                        ?>
                        <span class="input-group-addon success"><span class="glyphicon glyphicon-ok"></span></span>
                    </div>
                </div>

                <div class="form-group region" id="otherRegions" style="display:none;">
                    <label for="otherInfo" class="control-label"><?= __("Spécifiez") ?></label>
                    <div class="input-group">
                        <?= $this->Form->input('OtherRegion', ['label' => false, 'class' => 'form-control']); ?>
                        <span class="input-group-addon success"><span class="glyphicon glyphicon-remove"></span></span>
                    </div>
                </div>


                <div class="form-group">
                    <label for="postalCode" class="control-label"><?= __("Code postal") ?></label>
                    <div class="input-group">
                        <?= $this->Form->input('postalCode', ['label' => false, 'class' => 'form-control', 'required' => true]); ?>
                        <span class="input-group-addon danger"><span class="glyphicon glyphicon-remove"></span></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="control-label"><?= __("Adresse courriel") ?></label>
                    <div class="input-group" data-validate="email">
                        <?= $this->Form->input('email', ['label' => false, 'class' => 'form-control', 'required' => true]); ?>
                        <span class="input-group-addon danger"><span class="glyphicon glyphicon-remove"></span></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="emailConfirm" class="control-label"><?= __("Confirmation de l'adresse courriel") ?></label>
                    <div class="input-group" data-same="email">
                        <?= $this->Form->input('emailConfirm', ['label' => false, 'class' => 'form-control', 'required' => true]); ?>
                        <span class="input-group-addon danger"><span class="glyphicon glyphicon-remove"></span></span>
                    </div>
                </div>
            </div>
        </div>
    
        <?php if(count($specialFields) > 0): ?>
            <?= $this->element('Wizard/Account/special_fields', ['specialFields' => $specialFields]); ?>
        <?php endif ?>
    </form>

    <div id="contactFormAlert" class="alert alert-danger" role="alert" style="display:none;">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <span class="sr-only"><?= __("Erreur:") ?>:</span>
        <?= __("Veuillez compléter le formulaire de contact") ?>
    </div>
</section>