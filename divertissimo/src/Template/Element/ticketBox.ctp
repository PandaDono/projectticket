<div class="row">
    <div class="ticketBox col-md-12">
         <div class="col-md-3">
             <select class="form-control ticketDate">
                 <?php foreach($event['date'] as $index=>$date): ?>
                    <option value="<?= $index ?>"><?= $date->Display ?></option>
                 <?php endforeach ?>
             </select>
         </div>

        <div class="col-md-3">
            <select class="form-control ticketGrade">
            </select>
        </div>

        <div class="col-md-3">
            <select class="form-control ticketType">
            </select>
        </div>

         <div class="col-md-3" style="padding-right:0px; padding-left:0px;">
             <div class="col-xs-4">
                 <span class="ticketPrice" style="padding-right:0px; padding-left:0px;">0.00 $</span>
             </div>

             <div class="col-xs-4" style="padding-right:0px; padding-left:0px;">
                 <?= $this->Form->select(
                     'Nombre de billets',
                     ['0','1','2','3','4','5','6','7','8','9','10','11','12'],
                     ['class'=> 'form-control ticketQuantity', 'name' => 'ticketQty']
                 ); ?>
             </div>

             <div class="col-xs-4" style="padding-right:6px; padding-left:0px; text-align:right;">
                <span class="ticketTotal">0.00 $</span>
             </div>
         </div>
        <input type="hidden" class='ticketIdentifier' name="ticketIdentifier" value="" />
        <input type="hidden" class='stockQuantity' name="stockQuantity" value="" />
        <input type="hidden" class='ticketValue' name="stockQuantity" value="" />
    </div>
</div>
