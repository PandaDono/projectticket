<p>Merci beaucoup d’avoir utilisé Divertissimo.ca pour l’achat de vos billets.</p>

<p>Nous espérons que vous avez apprécié votre expérience d’achat et nous vous souhaitons un plaisir fou lorsque vous participerez à votre événement.</p>

<p>Vous trouverez dans le document en annexe une confirmation de votre transaction, ainsi que vos billets à imprimer si vous avez choisi la billetterie électronique.</p>

<p>Pour toute question ou commentaire en lien avec les services de Divertissimo.ca, nous vous invitons à communiquer avec nous au : info@divertissimo.ca</p>