<section class="standard">
	<h3>Inscrire un événement</h3>
	<p>Divertissimo.ca est un service offert GRATUITEMENT aux promoteurs et diffuseurs d’événements. </p>
	<p>Il donne accès à une importante banque de consommateurs par l’entreprise d’un portail esthétique et bien référencé. </p>
	<p>Il vous permet d'obtenir une présence sur le web gratuite ainsi qu'un service de billetterie complet.</p>

	<p>La vraie question est : pourquoi pas?</p>

	<p>Si vous souhaitez promouvoir votre événement via Divertissimo.ca, veuillez prendre contact avec nous par courriel à l'adresse suivante : promoteurs@divertissimo.ca</p>
</section>