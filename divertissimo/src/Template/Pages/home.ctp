<!--[if lt IE 9]>
	<?php $this->Html->script(['divertissimo/html5shiv', 'divertissimo/respond.min']) ?>
<![endif]-->
	<?php $this->start('bottomScript') ?>
		<script type="application/javascript">

			var eventGetterUrl = "<?= $this->Url->build(['controller' => 'Events', 'action' => 'findEvents']) ?>";

			$(document).ready(function(){

				var events = <?= $eventListing ?>;
				$("#searchName").autocomplete({
					 source: <?= $eventListing ?>,
					 minLength: 3
				 });

				$("#nameForm").submit(function(e){
					e.preventDefault();

					var searchText = $("#searchName").val();
					var searchCategory = $("#searchCategory").val();

					$.ajax({
						url: eventGetterUrl,
						type: 'GET',
						data: {search: searchText, category: searchCategory},
						dataType: 'html'
					})
					.done(function(data){
						$("#searchResults").html(data);
					});
				})

				$("#searchCategory").change(function(){
					$("#searchName").val('');
				})
			});

		</script>
	<?php $this->end() ?>

    <section id="home">
		<div id="main-slider" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#main-slider" data-slide-to="0" class="active"></li>
			</ol>
			<div class="carousel-inner">
				<div class="item active">
					<?= $this->Html->image('slider/rf_banner.jpg', ['class' => 'responsive']) ?>
					<div class="carousel-caption">
						<h2><?= __("RockFest de Saint-Pierre-Baptiste") ?></h2>
						<h4><?= __("Du 26 au 28 août 2016") ?></h4>
						<a href="/rockfest"><?= __('Acheter vos billets') ?><i class="fa fa-angle-right"></i></a>
					</div>
				</div>
			</div>
		</div>    	
    </section>

	<section id="search" class="standard">
		<div id="searchbox" style="padding:0px 30px;">
			<h3><?= __("Trouver un événement") ?></h3>
			<form id="nameForm">
				<div class="row">
					<div class="col-md-3">
						<input type="text" id="searchName" placeholder="Événement, lieu ou promoteur" size="50" class='form-control'/>
					</div>
					<div class="col-md-3">
						<?= $this->Form->select(
							'searchCategory',
							$activeCategories,
							['id' => 'searchCategory', 'class'=> 'form-control']
						); ?>
					</div>
					<div class="col-md-2">
						<button type="submit" class="btn btn-primary">
							Trouver
							<span class="glyphicon glyphicon-refresh spinning"  style="display:none; margin-left:20px;"></span>
						</button>
					</div>
				</div>
			</form>
		</div>
		<div class="row" id="searchResults">

		</div>
	</section>

	<div class="standard">
		<div style="padding:0px 30px;">
			<h2><?= __("Divertissimo c'est quoi?") ?></h2>
			<p style="font-weight:bold;">Que fait-on aujourd’hui?</p>
			<p style="font-weight:bold;">Qu’y a-t-il à faire dans la région?</p>
			<p style="font-weight:bold;">Où vivre une expérience qui sort de l’ordinaire?</p>

			<p>
				Voici autant de questions qui ne resteront plus sans réponse grâce à Divertissimo,
				un portail révolutionnaire vous donnant accès, en quelques clics,
				à une foule d’événements plus intéressants les uns que les autres.
			</p>

			<p>
				Grâce à Divertissimo, accédez à la liste la plus complète d’événements répertoriés
				par région et payez moins cher pour vos billets. Voici une innovation qui ne fait que des gagnants.
			</p>
		</div>
	</div>