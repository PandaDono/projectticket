<section class="standard">
	<p>1er mai 2016<p>

	<h3>Conditions d’utilisation </h3>

	<p>Bienvenue sur Divertissimo.ca! C’est bien connu, aussi séduisant et pertinent notre billetterie en ligne puisse être, nous nous devons d’instaurer un code de conduite. Voici les termes d’utilisation qui vous permettront d’obtenir une expérience optimale. </p>

	<p>Au sein de ce document fleuve, vous retrouverez notre politique de protection de la vie privée, notre politique d’achat, ainsi que les autres politiques qui régissent l’achat de billets. Vous trouverez des règles et codes de conduite que peu d’entre-vous liront, mais qui gouvernent tout de même notre façon de faire. En visitant notre site et en y achetant des billets, vous convenez implicitement à ces termes. </p>

	<p>Prenez note qu’il est possible que ce contrat entre notre site et le consommateur soit appelé à changer son préavis, Nous allons immédiatement en publier les modifications au sein de cette rubrique. La date située au haut de cette page vous informera de la dernière fois où un changement a été apporté aux termes d’utilisation. En continuant de visiter Divertissimo.ca après un changement, vous acceptez implicitement ce changement. </p>


	<p>Il est possible que certains des événements présentés sur notre site s’adressent particulièrement aux enfants. Veuillez noter malgré tout que ce site ne s’adresse pas aux enfants, qu’aucune publicité ne sera faite afin de les interpeller et que tout utilisateur de Divertissimo.ca doit être âgé de 13 ans et plus. </p>

	<h3>Création d’un compte Divertissimo.ca </h3>

	<p>Il est possible de naviguer son notre site et d’acheter des billets sans détenir un compte Divertissimo.ca. Nous vous suggérons toutefois de remplir le formulaire d’inscription lors de votre premier achat de billets afin de faciliter vos achats subséquents.  Veuillez noter que votre nom d’utilisateur et votre mot de passe ne seront accessibles à personne d’autre qu’à vous. Vous serez responsable de leur confidentialité. Vous ne pouvez vendre ou transférer l’accès à votre compte. Nous ne pouvons être tenus responsables des dommages causés par la divulgation de votre nom d’utilisateur ou votre mot de passe ou par leur utilisation par une tierce personne. Vous ne pouvez pas non plus utiliser le compte d’un autre membre sans sa permission explicite. </p>

	<p>Nous vous demandons de nous informer immédiatement si vous vous apercevez que quelqu’un utilise votre compte, ou tout autre compte, sans autorisation. Nous vous suggérons également de changer votre nom d’utilisateur et mot de passe dès que vous suspectez un problème de sécurité.
		Votre compte ne représente pas un titre de propriété, nous nous gardons le droit de refuser toute inscription, d’effacer un compte ou d’empêcher l’accès à notre billetterie, peu importe la raison. </p>

	<h3>Code de conduite </h3>

	<p>En utilisant notre site, vous acceptez de suivre toutes les lois et réglements en vigueur dans la province de Québec et vous vos engagez à ne pas : </p>
	<p>-Restreindre l’accès à notre site à quiconque; </p>
	<p>-Utiliser notre site à des dessins illégaux; </p>
	<p>-Soumettre de l’information ou du contenu qui ne respecte pas les lois, qui est frauduleux ou diffamatoire; </p>
	-Soumettre de l’information confidentielle concernant une tierce personne ou une compagnie; </p>
	<p>-Utiliser de façon frauduleuse un code de promotion qui ne vous concerne pas; </p>
	<p>-Utiliser quelconque partie de ce site à des fins commerciales ou de propagande; </p>

	<h3>Propriété du site et propriété intellectuelle </h3>

	<p>Ce site et toutes les informations qui s’y trouvent (textes, données, graphiques, images, photographies, code source, billets, etc.) sont la propriété de Divertissimo.ca et de ses partenaires. </p>

	</p>Nous et nos partenaires avons le droit d’auteur et clamons la propriété intellectuelle de tout ce qui se retrouve sur ce site.  Nous pouvons ainsi en changer le contenu en tout temps. Nous vous offrons ainsi un accès limité, conditionnel, gratuit, non exclusif et non transférable à l’information contenue sur notre site. Vous ne pouvez pas en faire l’utilisation à des fins commerciales.
	Vous ne pouvez utiliser les images, logos ou noms se retrouvant sur ce site sans en demander la permission écrite en <a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'contact']) ?>">communiquant avec nous</a>. </p>

	</p>Vous ne pouvez non plus modifier, adapter, vendre, désassembler ou pirater ce site sans faire l’objet de plaintes criminelles formelles. Nous interdisons également l’utilisation de robots, lecteurs hors ligne ou application de recherche manuelle ou automatique sur ce site. L’utilisation d’un logiciel automatisé pour réserver des billets ou obtenir des codes promotionnels est proscrite. </p>

	<p>Le fait d’accéder ou de rafraîchir une page d’événement ou d’achat de billets plus d’une fois dans un intervalle de trois secondes sera perçu comme une tentative de nuire au site, tout comme la demande d’accès à plus de 1000 pages dans un délai de 24 heures. </p>

	<p>Vous ne pouvez par reproduire un billet imprimé ou imprimer un billet sur un support qui aurait pour effet de le dénaturer. Il n’est pas permis non plus de décoder, décrypter, modifier les billets ou les algorithmes utilisés pour fournir le service. </p>

	<p>Toute utilisation contrevenant à cette politique aura pour effet de mettre un terme à votre autorisation d’utiliser ce site.

	<h3>Contrôle parental </h3>

	<p>Nous ne pouvons pas empêcher des mineurs d’accéder au site. Nous nous fions donc au jugement des parents ou des gardiens lorsque vient le temps de choisir le contenu à être affiché à l’écran et la prise de décision relativement à l’achat de billets. </p>

	<h3>Messages mobiles </h3>

	<p>Il est possible, si vous téléchargez notre application sur votre téléphone cellulaire ou si vous prenez part à un concours que nous organisons, que vous receviez un message texte ou une alerte sur votre téléphone. Si vous recevez un tel message, c’est que vous nous avons autorisé à le faire au préalable. </p>

	<p>Nous ne serons pas responsables pour les coûts qui peuvent vous être exigés par votre fournisseur de téléphonie cellulaire pour la réception de ces messages. Vous pouvez vous désabonner de notre service d’envoi de messages en tout temps en répondant le message « STOP » au texto reçu. Nous vous enverrons alors un dernier message confirmant votre requête. Vous pouvez également <a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'contact']) ?>">nous écrire</a> pour nous aviser de votre désir d’arrêter de recevoir des messages. </p>

	<p>Si vous avez installé notre application, vous recevrez des alertes ciblées en fonction des paramètres que vous avez sélectionnés. Vous pouvez modifier ces paramètres ou tout simplement mettre fin à la réception de ces messages en vous rendant dans la section « réglage » de votre téléphone. </p>

	<h3>Violation des conditions </h3>

	<p>Nous nous réservons le droit de faire enquête pour toute violation aux conditions décrites dans le présent document. Il est possible que nous prenions des mesures légales afin de faire respecter nos droits si nous le jugeons nécessaire. Une compensation en argent peut ne pas être suffisante pour empêcher une poursuite. </p>

	<h3>Déclaration de garantie </h3>

	<p>Nous offrons le site et son contenu « tel quel » et « tel que disponible ». Nous faisons tout en notre pouvoir afin que le site soit en ligne, sécuritaire et qu’il ne contienne pas de bogue. Quoi qu’il en soit, lorsque vous naviguez sur notre page, vous le faites à vos propres risques. </p>

	<p>Nous ne garantissons pas que le site sera toujours en fonction, sécuritaire ou sans erreur, il est possible qu’il y ait certains délais ou imperfections. Nous ne pouvons être tenus responsables des actions provenant des utilisateurs. </p>

	<h3>Limite de responsabilité </h3>

	<p>Divertissimo.ca et ses partenaires ne peuvent être tenus responsables de tout virus, cheval de Troie ou vers pouvant provenir de la navigation sur notre site. Nous ne pouvons pas non plus être tenus responsables d’un billet volé ou endommagé. Lorsqu’ils quittent nos installations, ces billets sont en parfait état. Il ne sera pas possible d’entamer des procédures judiciaires pour toute erreur ou impression se retrouvant sur le site ou lors de l’expédition des billets. Nous ferons le nécessaire pour corriger la situation rapidement. </p>

</section>