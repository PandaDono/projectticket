<section class="standard">
<h3>Divertissimo.ca</h3>
<p>
	Divertissimo est une entreprise familiale ayant à coeur le développement social, culturel, sportif et économique de leur région.
</p>
<p>
	La mission de Divertissimo.ca est d’offrir aux diffuseurs et promoteurs d’événements un accès GRATUIT
	à une billetterie en ligne afin d’améliorer leur achalandage. Il deviendra, d’un même élan, une référence
	en matière de divertissement auprès de la population.
</p>
<p>
	Véritable portail vers le divertissement, Divertissimo.ca sera à l’origine d’une synergie nouvelle entre la culture,
	le sport et la population. Il donnera un souffle nouveau aux promoteurs et diffuseurs d’événements tout en contribuant
	à améliorer la qualité de vie des gens.
</p>
</section>