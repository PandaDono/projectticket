<section class="standard">
	<h3>Informations de contact</h3>
	<p>N’hésitez pas à communiquer avec nous pour toute demande de renseignement. Voici nos coordonnées complètes :</p>

	<p>Divertissimo.ca <br/>
	124, Lucille-Lesieur <br/>
	Victoriaville (Québec) <br/>
	G6T 0X8
	</p>

	<p>Courriel : info@divertissimo.ca</p>
</section>