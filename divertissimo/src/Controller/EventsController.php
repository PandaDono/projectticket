<?php

namespace App\Controller;

use Cake\Controller\Controller;

class EventsController extends Controller
{
	public function initialize(){
		$this->loadModel('Events');
	}

	public function display($url)
	{
		$event = $this->Events->find('byUrl', ['url' => $url]);

		$this->set('event', $event);
		$this->set('jsonEvent', json_encode($event));
	}

	public function findEvents()
	{
		$this->viewBuilder()->layout(false);

		$searchText = $this->request->query['search'];
		$searchCategory = $this->request->query['category'];

		if(strpos($searchText,'événement') !== false){
			$searchText = str_replace('(événement)', '', $searchText);
			$events = $this->Events->find('byName', ['name' => $searchText, 'category' => $searchCategory]);
		}else if(strpos($searchText,'lieu') !== false){
			$searchText = str_replace('(lieu)', '', $searchText);
			$events = $this->Events->find('byPlaceName', ['name' => $searchText, 'category' => $searchCategory]);
		}else if(strpos($searchText,'promoteur') !== false){
			$searchText = str_replace('(promoteur)', '', $searchText);
			$events = $this->Events->find('byPromoterName', ['name' => $searchText, 'category' => $searchCategory]);
		}else{
			$events = $this->Events->find('byAnything', ['name' => $searchText, 'category' => $searchCategory]);
		}

		$this->set('events', $events);
	}
}