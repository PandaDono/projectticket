<?php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Core\Configure;
use Stripe\Stripe;
use Stripe\Charge;
use Stripe\Error\Card;


class TransactionsController extends Controller
{
    public function initialize()
    {
        $this->loadModel('Customers');
        $this->loadModel('SpecialFieldResults');
        $this->loadModel('Transactions');
        $this->loadModel('TransactionTickets');
        
        $this->loadComponent('Email');
        $this->loadComponent('Paypal');
    }

    public function stripe()
    {
        $this->autoRender = false;
        $contactInfo = $this->request->data['contact'];
        $order = $this->request->session()->read('order');
        $order->paymentMode = 'Stripe';

        $stripeToken = $this->request->data['token'];
        $contactInfo = $this->prepareArray($contactInfo);
        $contactInfo = $this->assignRegion($contactInfo);

        $paymentResult = $this->payWithStripe($stripeToken, $order, $contactInfo);

        if($paymentResult){
            $customer = $this->saveCustomer($contactInfo);
            $this->saveSpecialFields($contactInfo, $customer->customer_id);
            $transaction = $this->saveTransaction($order, $paymentResult->id, $customer);
            $tickets = $this->saveTransactionTickets($order, $transaction);

            $emailSent = $this->trySendingEmailConfirmation($customer, $transaction, $order);
            $transaction->transaction_email_sent = $emailSent;
            $this->Transactions->save($transaction);

            $confirmationCodes = "";
            foreach($tickets as $ticket)
            {
                $confirmationCodes .= $ticket->transaction_ticket_code . ' ';
            }

            $this->request->session()->write('confirmationCodes', $confirmationCodes);

            echo json_encode("success");
        }else{
            echo json_encode("error");
        }
    }

    private function payWithStripe($token, $order, $contactInfo)
    {
        $stripe_public = Configure::read('stripe_secret', 'default');
        $apiKey = $stripe_public[Configure::read('payment_env', 'default')];

        Stripe::setApiKey($apiKey);

        try {
            $charge = Charge::create(array(
                "amount" => $order->total * 100, // amount in cents, again
                "currency" => "cad",
                "source" => $token,
                "description" => "Achat divertissimo",
                "metadata" => ['name' => $contactInfo['firstname'] . ' ' . $contactInfo['lastname'] , 'email' => $contactInfo['email']]
            ));
            
            return $charge;
        } catch(Card $e) {
            return false;
        }
    }

    private function trySendingEmailConfirmation($customer, $transaction, $order)
    {
        $emailSent = $this->Email->sendEmailConfirmation($customer, $transaction, $order);
        return $emailSent;
    }

    public function paypal()
    {
        $this->autoRender = false;
        $contactInfo = $this->request->data['contact'];
        $order = $this->request->session()->read('order');

        $contactInfo = $this->prepareArray($contactInfo);
        $contactInfo = $this->assignRegion($contactInfo);

        $this->request->session()->write('contact', $contactInfo);

        $payment = $this->Paypal->createPaypalPayment($order);

        if($payment){
            $paymentLink = $payment->getApprovalLink();
            echo json_encode(["result" => "success", 'link' => $paymentLink]);
        }else{
            echo json_encode(["result" => "error"]);
        }
    }

    public function paid()
    {
        $this->autoRender = false;

        $paymentId = $this->request->query['paymentId'];

        $paymentSuccessful = $this->Paypal->executePaypalPayment();

        if($this->consumeToken($paymentId) && $paymentSuccessful){
            $contactInfo = $this->request->session()->read('contact');
            $order = $this->request->session()->read('order');
            $order->paymentMode = 'Paypal';
    
            $customer = $this->saveCustomer($contactInfo);
            $this->saveSpecialFields($contactInfo, $customer->customer_id);

            $transaction = $this->saveTransaction($order, $paymentId, $customer);
            $tickets = $this->saveTransactionTickets($order, $transaction);

            $emailSent = $this->trySendingEmailConfirmation($customer, $transaction, $order);
            $transaction->transaction_email_sent = $emailSent;
            $this->Transactions->save($transaction);

            $confirmationCodes = "";
            foreach($tickets as $ticket)
            {
                $confirmationCodes .= $ticket->transaction_ticket_code . ' ';
            }
    
            $this->request->session()->write('confirmationCodes', $confirmationCodes);
    
            $this->redirect(['controller' => 'Purchases', 'action' => 'confirmation']);
        }else{
            
        };
    }
    
    public function cancelled(){
        $this->request->session()->destroy();
    }

    public function error(){
        $this->request->session()->destroy();
    }

    private function consumeToken($paymentId){
        $this->LoadModel("Tokens");
        $token = $this->Tokens->findByToken($paymentId)->first();
        if($token && $token->consumed == false){
            $token->consumed = true;
            $this->Tokens->save($token);
            return true;
        }else{
            return false;
        }
    }

    private function saveCustomer($contactInfo)
    {
        $customer = $this->Customers->NewEntity();

        $customer->customer_firstname = $contactInfo['firstname'];
        $customer->customer_lastname = $contactInfo['lastname'];
        $customer->customer_phone = $contactInfo['phone'];
        $customer->customer_address = $contactInfo['address'];
        $customer->customer_city = $contactInfo['city'];
        $customer->customer_country = $contactInfo['country'];
        $customer->customer_region = $contactInfo['region'];
        $customer->customer_postalcode = $contactInfo['postalCode'];
        $customer->customer_email = $contactInfo['email'];

        $this->Customers->save($customer);
        
        $this->request->session()->write("customer", $customer);

        return $customer;
    }

    private function saveSpecialFields($fields, $customerId)
    {
        foreach($fields as $key=>$field){
            if(strpos($key, 'special') === 0){
                $fieldParts = explode('_', $key);

                $specialFieldResult = $this->SpecialFieldResults->newEntity();
                $specialFieldResult->special_field_id = $fieldParts[1];
                $specialFieldResult->customer_id = $customerId;
                $specialFieldResult->special_field_result_value = $field;

                $this->SpecialFieldResults->save($specialFieldResult);
            }
        }
    }

    private function saveTransaction($order, $paymentId, $customer)
    {
        $transaction = $this->Transactions->newEntity();

        $transaction->customer_id = $customer->customer_id;
        $transaction->event_id = $order->getEventId();
        $transaction->transaction_subtotal = $order->subtotal;
        $transaction->transaction_fees = $order->fees;
        $transaction->transaction_shipping = $order->shippingFees;
        $transaction->transaction_shipping_method = $order->shippingMethod;
        $transaction->transaction_total = $order->total;
        $transaction->transaction_taxes = $order->taxes;
        $transaction->transaction_fees_taxes = $order->feesTaxes;
        $transaction->transaction_paypal_confirmation = $paymentId;
        $transaction->transaction_payment_mode = $order->paymentMode;

        $this->Transactions->save($transaction);

        return $transaction;
     }

    private function saveTransactionTickets(&$order, $transaction)
    {
        $transactionTickets = [];

        foreach($order->items as $ticket){
            $transactionTicket = $this->TransactionTickets->newEntity();

            $transactionTicket->transaction_id = $transaction->transaction_id;
            $transactionTicket->ticket_id = $ticket->ticketId;
            $transactionTicket->ticket_quantity = $ticket->quantity;
            $transactionTicket->ticket_base_price = $ticket->unitPrice;
            $transactionTicket->ticket_promo = $ticket->unitPromo;
            $transactionTicket->transaction_ticket_code = $this->generateTicketCode();

            $ticket->transaction_code = $transactionTicket->transaction_ticket_code;

            $this->TransactionTickets->save($transactionTicket);

            $transactionTickets[] = $transactionTicket;
        }

        return $transactionTickets;
    }

    private function generateTicketCode()
    {
        sleep(1);
        $code = strtoupper(base_convert(time(), 10, 36));

        $codeExists = $this->TransactionTickets->findByTransactionTicketCode($code)->count();

        if($codeExists){
            return $this->generateTicketCode();
        }else{
            return $code;
        }
    }
    
    private function prepareArray($paymentInfo)
    {
        $processedInfo = [];

        foreach($paymentInfo as $index=>$info){
            $processedInfo[$info['name']] = $info['value'];
        }

        return $processedInfo;
    }

    private function assignRegion($contactInfo){
        if($contactInfo['country'] == 'Canada'){
            $contactInfo['region'] = $contactInfo['CanadaRegion'];
        }elseif($contactInfo['country'] == 'US'){
            $contactInfo['region'] = $contactInfo['USRegion'];
        }elseif($contactInfo['country'] == 'Other'){
            $contactInfo['region'] = $contactInfo['OtherRegion'];
        }
        
        unset($contactInfo['CanadaRegion']);
        unset($contactInfo['USRegion']);
        unset($contactInfo['OtherRegion']);
        
        return $contactInfo;
    }
}