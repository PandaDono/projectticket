<?php

namespace App\Controller;

use App\Form\AccountForm;
use App\Lib\Order;
use App\Lib\OrderItem;
use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Mailer\Email;
use App\Lib\PDFGenerator;


class PurchasesController extends Controller
{
	public function initialize(){
		$this->loadModel('Tickets');
		$this->loadModel('SetAsides');
		$this->loadModel('SpecialFields');
	}
	
	public function wizard()
	{
		$order = $this->request->session()->read('order');

		if(!$order){
			return $this->redirect(['_name' => 'homeUrl']);
		}
		
		$accountForm = New AccountForm();
		$event = $order->getEvent();

		$specialFields = $this->SpecialFields->findByEventId($event->event_id)->contain("SpecialFieldChoices")->all();

		$this->set('specialFields', $specialFields);
		$this->set('accountForm', $accountForm);
		$this->set('order', $order);
		$this->set('event', $event);

		$stripe_public = Configure::read('stripe_public', 'default');
		$apiKey = $stripe_public[Configure::read('payment_env', 'default')];
		
		$this->set('stripeKey', $apiKey);
	}

	/*public function test()
	{
		$order = new Order();

		$this->loadModel('Tickets');
		$ticket = $this->Tickets->get(9);

		$item = new OrderItem($ticket, 2);
		$item->promo = ['promocode_id' => '1','promocode_amount' => '5.00', 'promocode_message' => 'Crédit membre Desjardins'];
		$item->unitPromo = 5;
		$item->transaction_code = 'BBB55HH';
		
		$order->addItem($item);
		$order->addItem($item);
		$order->addItem($item);
		$order->addItem($item);
		$order->addItem($item);

		$order->calculateTotalsForCheckout();
		$order->paymentMode = 'credit';

		$this->loadModel('Customers');
		$customer = $this->Customers->get(4);

		$PDFGenerator = new PDFGenerator();
		$pdf = $PDFGenerator->confirmation($order, $customer);
	}*/

	public function confirmation()
	{
		$codes = $this->request->session()->read('confirmationCodes');
		$error = $this->request->session()->read('error');

		$this->set("codes", $codes);
		$this->set("error", $error);

		$this->request->session()->delete('order');
		$this->request->session()->delete('contact');
		$this->request->session()->delete('customer');
	}

	public function confirmationpdf()
	{
		$pdfLocation = $this->request->session()->read("pdfLocation");

		$this->response->file($pdfLocation);
		$this->response->header('Content-Disposition', 'inline');

		return $this->response;
	}

	public function validate()
	{
		$this->autoRender = false;

		$data = $this->request->data;
		$order = new Order();

		$requestedTickets = array_filter($data['tickets'], function($ticket){
			return ($ticket['Qty'] > 0);
		});

		$setAsides = [];
		$errors = [];

		foreach($requestedTickets as $ticketRequest){
			$ticket = $this->Tickets->get($ticketRequest['Id'], ['contain' => ['TicketGrade']]);
			$ticketGrade = $ticket->ticket_grade;
			
			if($ticketGrade->checkTicketsAvailable($ticketRequest['Qty'])){
				$setAsides[] = $this->createSetAside($ticketGrade, $ticketRequest['Qty']);
				$order->addItem(new OrderItem($ticket, $ticketRequest['Qty'], $ticketGrade));
			}else{
				$errors[] = __("Désolé, il ne reste plus assez de billets {0} de catégorie {1}.",
					[$ticket->ticket_name, $ticketGrade->ticket_grade_name]);
				break;
			}
		}

		if(empty($errors)){
			foreach($setAsides as $setAside){
				$this->SetAsides->save($setAside);
			}

			$order->calculateTotals($requestedTickets);

			$this->request->session()->write('order', $order);

			echo json_encode(['ok']);
		}else{
			echo json_encode($errors);
		}
	}

	public function promocode()
	{
		$this->autoRender = false;

		$this->loadModel('Promocodes');

		$order = $this->request->session()->read('order');
		$code = $this->request->query['code'];
		$result = null;
		
		$tickets = $order->items;

		foreach($tickets as &$ticket){
			$promo = $this->Promocodes->find('ByCodeAndTicket', ['code' => $code, 'ticketId' => $ticket->ticketId]);

			if($promo){
				$ticket->unitPromo = $promo->promocode_amount;
				$ticket->promo = $promo;
				$result = ['message' => $promo['promocode_message'] . __(' appliqué'), 'success' => true];
			}
		}
		
		if(!$result){
			$result = ['message' => __("Le code entré ne s'applique à aucun de vos achats."), 'success' => false];
		}

		$order->calculateTotalsForCheckout();
		$this->request->session()->write('order', $order);

		echo json_encode($result);
	}
	
	public function totals()
	{
		$this->autoRender = false;

		$ticketsData = $this->request->query['tickets'];

		$order = New Order();
		$order->calculateTotals($ticketsData);
		
		echo json_encode($order);
	}

	public function shipping()
	{
		$this->autoRender = false;

		$shippingMethod = $this->request->data['shippingMethod'];
		$order = $this->request->session()->read('order');
		$order->adjustShipping($shippingMethod);
		$this->request->session()->write('order', $order);
		
		echo json_encode($order);
	}

	public function promototal()
	{
		$this->autoRender = false;

		$order = $this->request->session()->read('order');
		$order->calculateTotalsForCheckout();
		$this->request->session()->write('order', $order);

		echo json_encode($order);
	}
	
	private function createSetAside($ticketGrade, $quantity)
	{
		$setAside = $this->SetAsides->NewEntity();
		$setAside->ticket_grade_id = $ticketGrade->ticket_grade_id;
		$setAside->quantity = $quantity;

		return $setAside;
	}

}