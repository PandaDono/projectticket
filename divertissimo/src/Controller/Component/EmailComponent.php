<?php

namespace App\Controller\Component;

use App\Lib\PDFGenerator;
use Cake\Controller\Component;
use Cake\Log\Log;
use Cake\Mailer\Email;

class EmailComponent extends Component
{
    public function sendEmailConfirmation($customer, $transaction, $order)
    {
        $pdfFile = 'Divertissimo' . $transaction->transaction_id . '_'. $transaction->customer_id .'.pdf';
        $pdfPath = WWW_ROOT . 'pdf' . DS . $pdfFile;
        $PDFGenerator = new PDFGenerator($pdfPath, 'F');
        $PDFGenerator->confirmation($order, $customer);

        $this->request->session()->write("pdfLocation", $pdfPath);

        $email = new Email('default');
        $email->from(['billetterie@divertissimo.ca' => 'Divertissimo'])
            ->to($customer->customer_email)
            //->bcc('ypoisson@latribune.qc.ca')
            ->subject("Confirmation d'achat")
            ->template('confirmation', 'default')
            ->emailFormat('html')
            ->attachments([
                $pdfFile => [
                    'file' => $pdfPath,
                    'mimetype' => 'application/pdf'
                ]
            ]);
        try{
            $email->send();
            return true;
        }catch(\Exception $ex){
            Log::write('error', $ex);
            return false;
        }
    }
}