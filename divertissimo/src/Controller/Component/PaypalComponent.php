<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use PayPal\Api\Amount;
use PayPal\Api\CreditCard;
use PayPal\Api\Details;
use PayPal\Api\FundingInstrument;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\PaymentExecution;
use Paypal\Exception\PayPalConnectionException;

use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

class PaypalComponent extends Component
{
	private $clientId = 'Aa2JXjM8gWN-HFtcZALC0AKzkA6YfEUod2P4LP7WMuepFGlF_WgowM-4Lzr7HMLxjIC1Iuc6JQScTsQG';
	private $clientSecret = 'EPbyLZurZNXH_k42LbYotTwFUAmKH2CAxLm-JuykhrQeeCBzyCeuo-EaSV6SRfdRUoR3F31s6jQl7kFx';

	public function initialize(array $config)
	{

	}
	
	public function createPayment($payment, $order)
	{
		$apiContext = $this->getApiContext();

		$card = new CreditCard();
		$card->setType($payment['cardName'])
			->setNumber($payment['cardNumber'])
			->setExpireMonth($payment['expirationMonth'])
			->setExpireYear($payment['expirationYear'])
			->setCvv2($payment['cardSafety'])
			->setFirstName($payment['ownerFirstname'])
			->setLastName($payment['ownerLastname']);

		$fi = new FundingInstrument();
		$fi->setCreditCard($card);

		$payer = new Payer();
		$payer->setPaymentMethod("credit_card")
			->setFundingInstruments(array($fi));

/*		$paypalItems = [];
		foreach($order->items as $item){
			$PaypalItem = new Item();
			$PaypalItem->setName($item->eventName)
				->setDescription($item->getFullReference())
				->setCurrency('CAD')
				->setQuantity($item->quantity)
				->setTax($item->unitPrice - $item->taxlessPrice)
				->setPrice($item->taxlessPrice);

			$paypalItems[] = $PaypalItem;
		}

		$PaypalItem = new Item();
		$PaypalItem->setName("Frais billetterie")
			->setCurrency('CAD')
			->setQuantity(1)
			->setTax($order->feesTaxes)
			->setPrice($order->fees);

		$paypalItems[] = $PaypalItem;

		$itemList = new ItemList();
		$itemList->setItems($paypalItems);
*/

		$details = new Details();
		$details->setShipping($order->shippingFees)
			->setTax($order->taxes)
			->setSubtotal($order->subtotal + $order->fees);

		$amount = new Amount();
		$amount->setCurrency("CAD")
			->setTotal($order->total)
			->setDetails($details);

		$transaction = new Transaction();
		$transaction->setAmount($amount)
			//->setItemList($itemList)
			->setDescription("Achat Divertissimo")
			->setInvoiceNumber(uniqid());

		$payment = new Payment();
		$payment->setIntent("sale")
			->setPayer($payer)
			->setTransactions(array($transaction));

		try {
			return $payment->create($apiContext);
		} catch (PayPalConnectionException $ex) {
			//Log::write('error', $ex);
			return false;
		}
	}

	public function createPaypalPayment($order)
	{
		$apiContext = $this->getApiContext();

		$payer = new Payer();
		$payer->setPaymentMethod("paypal");

		$details = new Details();
		$details->setShipping($order->shippingFees)
			->setTax($order->taxes)
			->setSubtotal($order->subtotal + $order->fees);

		$amount = new Amount();
		$amount->setCurrency("CAD")
			->setTotal($order->total)
			->setDetails($details);

		$transaction = new Transaction();
		$transaction->setAmount($amount)
			//->setItemList($itemList)
			->setDescription("Achat Divertissimo")
			->setInvoiceNumber(uniqid());

		$successUrl = Router::url(['controller' => 'Transactions', 'action' => 'paid'], true);
		$cancelUrl = Router::url(['controller' => 'Transactions', 'action' => 'cancelled'], true);

		$redirectUrls = new RedirectUrls();
		$redirectUrls->setReturnUrl($successUrl)
			->setCancelUrl($cancelUrl);

		$payment = new Payment();
		$payment->setIntent("sale")
			->setPayer($payer)
			->setRedirectUrls($redirectUrls)
			->setTransactions(array($transaction));

		try {
			$payment->create($apiContext);
			$this->saveToken($payment);
			return $payment;
		} catch (PayPalConnectionException $ex) {
			//Log::write('error', $ex);
			return false;
		}
	}
	
	public function executePaypalPayment()
	{
		$apiContext = $this->getApiContext();
		
		$paymentId = $_GET['paymentId'];
		$payment = Payment::get($paymentId, $apiContext);
		
		$execution = new PaymentExecution();
		$execution->setPayerId($_GET['PayerID']);

		try {
			$result = $payment->execute($execution, $apiContext);
			return $result;
		} catch (Exception $ex) {
			return false;
		}
	}

	private function getApiContext()
	{
		$apiContext = new ApiContext(
			new OAuthTokenCredential(
				$this->clientId,
				$this->clientSecret
			)
		);

		$apiContext->setConfig(
			array(
				'mode' => 'sandbox', //LIVE
				'log.LogEnabled' => true,
				'log.FileName' => '../logs/PayPal.log',
				'log.LogLevel' => 'FINE', // PLEASE USE `FINE` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
				'cache.enabled' => true,
				// 'http.CURLOPT_CONNECTTIMEOUT' => 30
				// 'http.headers.PayPal-Partner-Attribution-Id' => '123123123'
			)
		);

		return $apiContext;
	}

	private function saveToken($payment)
	{
		$tokenRepository = TableRegistry::get("Tokens");
		$token = $tokenRepository->newEntity();
		$token->token = $payment->getId();
		$tokenRepository->save($token);
	}
}