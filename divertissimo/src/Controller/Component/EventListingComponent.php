<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

class EventListingComponent extends Component
{
	public function getJSONListing($eventRepository)
	{
		$results = $eventRepository->find('AllActiveEvents');
		$results = $this->flattenSearchResults($results);

		return $results;
	}

	public function getActiveCategories($eventRepository)
	{
		$results = $eventRepository->find('AllActiveCategories');

		$processedResults = [];

		$processedResults[''] = 'Choissisez une catégorie';

		foreach($results as $result){
			$processedResults[$result['Category']['category_id']] = $result['Category']['category_name'];
		}

		return array_unique($processedResults);
	}

	private function flattenSearchResults($results)
	{
		$processedResults = [];

		foreach($results as $result){
			$processedResults[] = $result['event_name'] . " (événement)";
			$processedResults[] = $result['place']['place_name'] . " (lieu)";
			$processedResults[] = $result['promoter']['promoter_name'] . " (promoteur)";
		}

		return array_unique($processedResults);
	}
}