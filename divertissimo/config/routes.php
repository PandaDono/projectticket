<?php

use Cake\Core\Plugin;
use Cake\Routing\Router;

Router::defaultRouteClass('DashedRoute');

Router::scope('/', function ($routes) {
    $routes->connect('/', ['controller' => 'Pages', 'action' => 'home'], ['_name' => 'homeUrl']);
    $routes->connect('/demo', ['controller' => 'Pages', 'action' => 'home']);

    $routes->connect('/english', ['controller' => 'App', 'action' => 'changeLanguage', 'lang' => 'en_CA']);
    
    $routes->connect('/:controller/:action', []);
    $routes->connect('/:url', ['controller' => 'Events', 'action' => 'display'],['pass' => ['url']]);

    $routes->fallbacks('DashedRoute');
});

/**
 * Load all plugin routes.  See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();
