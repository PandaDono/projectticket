<section class="standard">
    <?php foreach($events as $event): ?>
        <a href="<?= $this->Url->build(['action' => 'event', $event->event_id]) ?>">
            <button type="button" class="btn btn-primary btn-lg"><?= $event->event_name ?></button>
        </a>
    <?php endforeach ?>
</section>