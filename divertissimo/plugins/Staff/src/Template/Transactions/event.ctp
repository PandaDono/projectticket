<?php
echo $this->Html->script(['Staff.jquery.bootgrid']);
echo $this->Html->css(['Staff.jquery.bootgrid.min'])
?>

<?php $this->append('bottomScript') ?>
<script type="application/javascript">
    $(document).ready(function() {
        var grid = $("#grid-data").bootgrid({
            ajax: true,
            post: function ()
            {
                return {
                    eventId: <?= $eventId ?>
                };
            },
            url: "<?= $this->Url->build(['action' => 'transactions']) ?>",
            formatters: {
                "commands": function(column, row)
                {
                    return "<button type='button' class='btn btn-default btn-sm resend_button' title='Envoyer le courriel'><span class='glyphicon glyphicon-repeat' aria-hidden='true'></span> </button>" +
                        "<button type='button' class='btn btn-default btn-sm print_button' title='Imprimer la confirmation'><span class='glyphicon glyphicon-print' aria-hidden='true'></span></button>" +
                        "<button type='button' class='btn btn-default btn-sm mail_button' title='Marquer billets envoyés'><span class='glyphicon glyphicon-envelope' aria-hidden='true'></span></button>";
                    ;
                }
            }
        }).on("loaded.rs.jquery.bootgrid", function()
        {
            grid.find(".resend_button").on("click", function(e)
            {
                var row = $(this).closest("tr");
                var id = $(row).find('td').eq(0).text();

                resentEmail(id);
            }).end().find(".print_button").on("click", function(e)
            {
                var row = $(this).closest("tr");
                var id = $(row).find('td').eq(0).text();

                getPDF(id);
            }).end().find(".mail_button").on("click", function(e)
            {
                var row = $(this).closest("tr");
                var id = $(row).find('td').eq(0).text();

                markTicketsSent(id);
            });
        });
    });

    function resentEmail(id)
    {
        $("body").css("cursor", "wait");

        $.ajax({
                url: "<?= $this->Url->build(['action' => 'resendEmail']) ?>",
                type: 'POST',
                data: {id: id},
                dataType: 'text'
            })
            .success(function(result){
                $("#grid-data").bootgrid("reload")
            })
            .error(function(){
                alert("Une erreur s'est produite mautadine");
            })
            .done(function(data){
                $("body").css("cursor", "default");
            });
    }

    function getPDF(id)
    {
        var url = "<?= $this->Url->build(['action' => 'getTransactionPdf']) ?> ?id=" + id;
        window.open(url,'_blank');
    }
    
    function markTicketsSent(id)
    {
        $("body").css("cursor", "wait");

        $.ajax({
                url: "<?= $this->Url->build(['action' => 'markTicketsSent']) ?>",
                type: 'POST',
                data: {id: id},
                dataType: 'text'
            })
            .success(function(result){
                $("#grid-data").bootgrid("reload")
            })
            .error(function(){
                alert("Une erreur s'est produite mautadine");
            })
            .done(function(data){
                $("body").css("cursor", "default");
            });
    }

</script>
<?php $this->end(); ?>

<section class="standard">

    <h2><?= $eventName ?></h2>

    <table id="grid-data" class="table table-condensed table-hover table-striped" style="background-color:grey;">
        <thead>
        <tr>
            <th data-column-id="transaction_id">Id</th>
            <th data-column-id="transaction_date" data-type="date">Date</th>
            <th data-column-id="customer.customer_firstname">Prénom</th>
            <th data-column-id="customer.customer_lastname">Nom</th>
            <th data-column-id="transaction_shipping_method">Billets envoyés</th>
            <th data-column-id="transaction_email_sent">Courriel envoyé</th>

            <th data-column-id="commands" data-formatter="commands" data-sortable="false">Commands</th>
        </tr>
        </thead>
        <tbody style="color:black;">
        </tbody>
    </table>
</section>