<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Divertissimo</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css(
        [
            'jquery-ui.min', 'bootstrap.min', 'font-awesome.min',
            'main', 'Staff.staff', 'animate', 'responsive'
        ]
    ) ?>
    <?= $this->Html->script(
        [
            'jquery.min', 'bootstrap.min', 'jquery-ui.min','smoothscroll',
            'jquery.parallax', 'jquery.scrollTo',
            'jquery.nav'
        ],
        ['block' => true]
    ) ?>

    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

</head>
<body>
    <header id="header" role="banner" style="height:110px; margin-top:30px;">
        <div class="main-nav">
            <div class="container">
                <!--<div class="header-top">
                    <div class="pull-right social-icons">
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                        <a href="#"><i class="fa fa-youtube"></i></a>
                    </div>
                </div>
                -->
                <div class="row">

                    <div class="col-xs-2">
                        <a href="<?= $this->Url->build(['_name' => 'homeUrl']) ?>">
                            <?= $this->Html->image('logo2.png', ['class' => 'responsive logo'])  ?>
                        </a>
                    </div>
                    <div class="collapse navbar-collapse col-xs-10">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="<?= $this->Url->build(['_name' => 'homeUrl']) ?>"><?= __('Accueil') ?></a></li>
                            <li class="scroll"><a href="<?= $this->Url->build(['_name' => 'homeUrl']) ?>#search"><?= __('Achat de billets') ?></a></li>
                            <li class="scroll"><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'promote']) ?>"><?= __('Inscrire un événement') ?></a></li>
                            <li>
                                <?= $this->Html->link(
                                    __('À propos de nous'),
                                        [
                                            'controller' => 'Pages',
                                            'action' => 'about'
                                        ]
                                )?>
                            </li>
                            <li class="scroll"><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'contact']) ?>"><?= __('Contact') ?></a></li>
                            <!--<li class="scroll"><a href="#"><?= __('English') ?></a></li>-->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>
    <footer id="footer">
        <div class="container">
            <div class="text-center">
                <span>Copyright &copy;2016 Divertissimo Inc</span>
                <span style="float:right;"><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'conditions']) ?>">Conditions d'utilisation</a></span>
            </div>
        </div>
    </footer>

    <?= $this->fetch('bottomScript') ?>
</body>

</html>
