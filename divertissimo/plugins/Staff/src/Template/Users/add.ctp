<section class="standard">
    <div>
        <?= $this->Form->create($user) ?>
        <fieldset>
            <legend><?= __('Add User') ?></legend>
            <?= $this->Form->input('username', ['style'=>"color:black;"]) ?>
            <?= $this->Form->input('password', ['style'=>"color:black;"]) ?>
            <?= $this->Form->select('role',
                ['admin' => 'Admin', 'superadmin' => 'SuperAdmin'],
                ['style'=>"color:black;"]
            ); ?>
        </fieldset>
        <?= $this->Form->button(__('Submit')); ?>
        <?= $this->Form->end() ?>
    </div>
</section>