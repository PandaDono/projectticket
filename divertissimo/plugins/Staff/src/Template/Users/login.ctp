<section class="standard">

    <div class="users form loginForm">
        <?= $this->Flash->render() ?>
        <?= $this->Form->create() ?>
        <fieldset>
            <legend style="color:white;"><?= __("Merci d'entrer vos nom d'utilisateur et mot de passe") ?></legend>
            <?= $this->Form->input('username') ?>
            <?= $this->Form->input('password') ?>
        </fieldset>
        <?= $this->Form->button(__('Connexion'), ['class' => 'btn btn-default', 'style' => 'margin-top: 30px;']); ?>
        <?= $this->Form->end() ?>
    </div>
</section>