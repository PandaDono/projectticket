<?php

namespace Staff\Controller;

use App\Lib\Order;
use App\Lib\PDFGenerator;
use Cake\Event\Event;

class DashboardController extends AppController
{
	public function initialize()
	{
		parent::initialize();
	}

	public function beforeFilter(Event $event)
	{
		parent::beforeFilter($event);
		$this->Auth->allow(['login', 'add']);
	}

	public function login()
	{
		if ($this->request->is('post')) {
			$user = $this->Auth->identify();
			if ($user) {
				$this->Auth->setUser($user);
				return $this->redirect($this->Auth->redirectUrl());
			}
			$this->Flash->error(__("Nom d'usager ou mot de passe invalide"));
		}
	}

	public function logout()
	{
		return $this->redirect($this->Auth->logout());
	}

	public function index()
	{
	}

	public function transactions()
	{
	}

	public function transactionjson()
	{
		$this->autoRender = false;
		$this->loadModel('Transactions');

		//current=1&rowCount=10&sort[sender]=asc&searchPhrase=&id=b0df282a-0d67-40e5-8558-c9e93b7befed  //TODO CA
		

		$transactions = $this->Transactions->find()->contain('Customer')->contain('TransactionTickets')->all();

		$transactionsDisplay = [];
		foreach($transactions as $transaction){
			$transactionsDisplay[] = $this->buildTransactionDisplay($transaction);
		}

		$response = [
			'current' => 1,
			'rowCount' => 10,
			'rows' => $transactionsDisplay,
			'total' => count($transactionsDisplay)
		];

		$response = json_encode($response);

		echo $response;
	}

	public function resendEmail()
	{
		$this->autoRender = false;

		$this->loadModel('Transactions');
		
		$id = $this->request->data['id'];

		$transaction = $this->Transactions->findByTransactionId($id)->contain('Customer')->contain('TransactionTickets')->first();

		$customer = $transaction->customer;
		
		$order = new Order();
		$order->generateFromTransaction($transaction);

		$mailSent= $this->Email->sendEmailConfirmation($customer, $transaction, $order);

		if($mailSent){
			$transaction->transaction_email_sent = 1;
			$this->Transactions->save($transaction);
		}
		
		return "ok";
	}

	public function getTransactionPdf()
	{
		$this->loadModel('Transactions');

		$this->autoRender = false;

		$id = $this->request->query['id'];

		$transaction = $this->Transactions->findByTransactionId($id)->contain('Customer')->contain('TransactionTickets')->first();

		$customer = $transaction->customer;

		$order = new Order();
		$order->generateFromTransaction($transaction);

		$pdfGenerator = new PDFGenerator($id . '.pdf', 'D');
		$pdfGenerator->confirmation($order, $customer);
	}

	private function buildTransactionDisplay($transaction)
	{
		return [
			'id' => $transaction->transaction_id,
			'date' => $transaction->transaction_date,
			'firstname' => $transaction->customer->customer_firstname,
			'lastname' => $transaction->customer->customer_lastname,
			'email_sent' => $transaction->VerboseEmailSent
		];
	}

	public function add()
	{
		$user = $this->Users->newEntity();
		if ($this->request->is('post')) {
			$user = $this->Users->patchEntity($user, $this->request->data);
			if ($this->Users->save($user)) {
				$this->Flash->success(__('The user has been saved.'));
				return $this->redirect(['action' => 'add']);
			}
			$this->Flash->error(__('Unable to add the user.'));
		}
		$this->set('user', $user);
	}
}