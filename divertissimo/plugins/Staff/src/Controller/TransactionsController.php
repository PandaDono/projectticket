<?php
namespace Staff\Controller;

use App\Lib\Order;
use App\Lib\PDFGenerator;
use Staff\Controller\AppController;

class TransactionsController extends AppController
{
	public function initialize()
	{
		parent::initialize();
		$this->loadComponent('Email');
		$this->loadModel('Events');
		$this->loadModel('Transactions');
	}
	
	public function index()
	{
		$events = $this->Events->find()->all();
		$this->set('events', $events);
	}
	
	public function event($eventId)
	{
		$event = $this->Events->get($eventId);

		$this->set("eventName", $event->event_name);
		$this->set("eventId", $eventId);
	}
	
	public function transactions()
	{
		$this->autoRender = false;

		$eventId = $this->request->data['eventId'];
		$page = $this->request->data['current'];
		$rows = $this->request->data['rowCount'];
		$search = $this->request->data['searchPhrase'];

		if(isset($this->request->data['sort'])){
			$sort = $this->request->data['sort'];
		}else{
			$sort = ['transaction_id' => 'desc'];
		}

		$skip = ($page - 1) * $rows;
		
		$transactions = $this->Transactions->find('ByEventForAdmin', 
			[
				'eventId' => $eventId,
				'searchString' => $search, 
				'order' => $sort, 
				'limit' => $rows, 
				'offset' => $skip
			]
		);
		$transactionCount = $this->Transactions->find('CountByEventForAdmin',
			[
				'eventId' => $eventId,
				'searchString' => $search,
				'order' => $sort,
				'limit' => $rows,
				'offset' => $skip
			]
		);

		$transactionsDisplay = [];
		foreach($transactions as $transaction){
			$transactionsDisplay[] = $this->buildTransactionDisplay($transaction);
		}

		$response = [
			'current' => (int)$page,
			'rowCount' => (int)$rows,
			'rows' => $transactionsDisplay,
			'total' => (int)$transactionCount
		];

		$response = json_encode($response);

		echo $response;
	}

	public function resendEmail()
	{
		$this->autoRender = false;

		$id = $this->request->data['id'];

		$transaction = $this->Transactions->findByTransactionId($id)->contain('Customer')->contain('TransactionTickets')->first();

		$customer = $transaction->customer;
		
		$order = new Order();
		$order->generateFromTransaction($transaction);

		$mailSent= $this->Email->sendEmailConfirmation($customer, $transaction, $order);

		if($mailSent){
			$transaction->transaction_email_sent = 1;
			$this->Transactions->save($transaction);
		}
	}

	public function getTransactionPdf()
	{
		$this->autoRender = false;

		$id = $this->request->query['id'];

		$transaction = $this->Transactions->findByTransactionId($id)->contain('Customer')->contain('TransactionTickets')->first();

		$customer = $transaction->customer;

		$order = new Order();
		$order->generateFromTransaction($transaction);

		$pdfGenerator = new PDFGenerator($id . '.pdf', 'D');
		$pdfGenerator->confirmation($order, $customer);
	}

	public function markTicketsSent()
	{
		$this->autoRender = false;

		$id = $this->request->data['id'];
		$transaction = $this->Transactions->get($id);

		$transaction->transaction_tickets_sent = 1;
		$this->Transactions->save($transaction);
	}

	private function buildTransactionDisplay($transaction)
	{
		return [
			'transaction_id' => $transaction->transaction_id,
			'transaction_date' => $transaction->Date,
			'customer.customer_firstname' => $transaction->customer->customer_firstname,
			'customer.customer_lastname' => $transaction->customer->customer_lastname,
			'transaction_shipping_method' => $transaction->VerboseShipping,
			'transaction_email_sent' => $transaction->VerboseEmailSent
		];
	}

	public function add()
	{
		$user = $this->Users->newEntity();
		if ($this->request->is('post')) {
			$user = $this->Users->patchEntity($user, $this->request->data);
			if ($this->Users->save($user)) {
				$this->Flash->success(__('The user has been saved.'));
				return $this->redirect(['action' => 'add']);
			}
			$this->Flash->error(__('Unable to add the user.'));
		}
		$this->set('user', $user);
	}
}