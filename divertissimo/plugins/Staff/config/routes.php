<?php
use Cake\Routing\Router;

Router::plugin(
    'Staff',
    ['path' => '/staff'],
    function ($routes) {
        $routes->fallbacks('DashedRoute');
    }
);
